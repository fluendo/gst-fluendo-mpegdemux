/* To build:
 * gcc -Wall $(pkg-config --cflags --libs gstreamer-0.10) info.c -o info
 */

#include <gst/gst.h>
#include <glib.h>

static gboolean
bus_call (GstBus     *bus,
          GstMessage *msg,
          gpointer    data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}


static void
on_pad_added (GstElement *element,
              GstPad     *pad,
              gpointer    data)
{
  GstPad *sinkpad;
  GstElement *decoder = (GstElement *) data;

  sinkpad = gst_element_get_static_pad (decoder, "sink");
  if (!gst_pad_is_linked (sinkpad))
    gst_pad_link (pad, sinkpad);

  gst_object_unref (sinkpad);
}

static void
dump_descriptors (GValueArray *descriptors)
{
  GValue * value;
  gint i;
  for (i = 0 ; i < descriptors->n_values; i++) {
    GString *string;
    value = g_value_array_get_nth (descriptors, i);
    string = (GString *) g_value_get_boxed (value);
    
    if (string->len > 2) {
      g_print ("descriptor # %d tag %02x len %d\n",
          i + 1, (guint8)string->str[0], (guint8)string->str[1]);
      gst_util_dump_mem ((guint8*)string->str + 2, string->len - 2);    
    }
  }  
  g_print ("\n");
}

static void
dump_languages (GValueArray *languages)
{
  GValue * value;
  gint i;
  if (!languages->n_values)
    return;
 
  g_print ("languages: ");
  for (i = 0 ; i < languages->n_values; i++) {
    const gchar *string;
    value = g_value_array_get_nth (languages, i);
    string = g_value_get_string (value);    
    g_print ("%s,", string);
  }  
  g_print ("\n");
}

static void
demuxer_notify_pat_info (GObject *obj, GParamSpec *pspec, gpointer user_data)
{
  GValueArray *patinfo = NULL;
  GValue * value = NULL;
  GObject *entry = NULL;
  guint program, pid;
  gint i;

  g_object_get (obj, "pat-info", &patinfo, NULL);
  
  g_print ("PAT: entries: %d\n", patinfo->n_values);  
  for (i = 0; i < patinfo->n_values; i++) {
    value = g_value_array_get_nth (patinfo, i);
    entry = (GObject*) g_value_get_object (value);
    g_object_get (entry, "program-number", &program, NULL);
    g_object_get (entry, "pid", &pid, NULL);
    g_print ("    program: %04x pid: %04x\n", program, pid);
  }
}

static void
demuxer_notify_pmt_info (GObject *obj, GParamSpec *pspec, gpointer user_data)
{
  GObject *pmtinfo = NULL, *streaminfo = NULL;
  GValueArray *streaminfos = NULL;
  GValueArray *descriptors = NULL;
  GValueArray *languages = NULL;
  gint i;
  GValue * value;
  guint program, version, pcr_pid, es_pid, es_type;
  
  g_object_get (obj, "pmt-info", &pmtinfo, NULL);
  g_object_get (pmtinfo, "program-number", &program, NULL);
  g_object_get (pmtinfo, "version-number", &version, NULL);
  g_object_get (pmtinfo, "pcr-pid", &pcr_pid, NULL);
  g_object_get (pmtinfo, "stream-info", &streaminfos, NULL);
  g_object_get (pmtinfo, "descriptors", &descriptors, NULL);

  g_print ("PMT: program: %04x version: %d pcr: %04x streams: %d "
      "descriptors: %d\n",
      (guint16)program, version, (guint16)pcr_pid, streaminfos->n_values,
      descriptors->n_values);

  dump_descriptors (descriptors);
  for (i = 0 ; i < streaminfos->n_values; i++) {
    value = g_value_array_get_nth (streaminfos, i);
    streaminfo = (GObject*) g_value_get_object (value);
    g_object_get (streaminfo, "pid", &es_pid, NULL);
    g_object_get (streaminfo, "stream-type", &es_type, NULL);
    g_object_get (streaminfo, "languages", &languages, NULL);
    g_object_get (streaminfo, "descriptors", &descriptors, NULL);
    g_print ("pid: %04x type: %x languages: %d descriptors: %d\n",
        (guint16)es_pid, (guint8) es_type, languages->n_values,
        descriptors->n_values);
    dump_languages (languages);
    dump_descriptors (descriptors);
  }
}

int
main (int argc, char *argv[])
{
  GMainLoop *loop;

  GstElement *pipeline, *source, *demuxer, *sink;
  GstBus *bus;

  /* Initialisation */
  gst_init (&argc, &argv);

  loop = g_main_loop_new (NULL, FALSE);

  /* Check input arguments */
  if (argc != 2) {
    g_printerr ("Usage: %s <MPEG TS filename>\n", argv[0]);
    return -1;
  }

  /* Create gstreamer elements */
  pipeline = gst_pipeline_new ("test");
  source   = gst_element_factory_make ("filesrc", "file-source");
  demuxer  = gst_element_factory_make ("flutsdemux", "demuxer");
  sink     = gst_element_factory_make ("fakesink", "sink");

  if (!pipeline || !source || !demuxer || !sink) {
    g_printerr ("One element could not be created. Exiting.\n");
    return -1;
  }

  /* Set up the pipeline */

  /* we set the input filename to the source element */
  g_object_set (G_OBJECT (source), "location", argv[1], NULL);

  /* we add a message handler */
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref (bus);

  /* we add all elements into the pipeline */
  gst_bin_add_many (GST_BIN (pipeline),
                    source, demuxer, sink, NULL);

  /* we link the elements together */
  gst_element_link (source, demuxer);
  g_signal_connect (demuxer, "pad-added", G_CALLBACK (on_pad_added), sink);
  g_signal_connect(G_OBJECT(demuxer), "notify::pat-info", 
      (GCallback)demuxer_notify_pat_info, NULL);
  g_signal_connect(G_OBJECT(demuxer), "notify::pmt-info", 
      (GCallback)demuxer_notify_pmt_info, NULL);

  /* Set the pipeline to "playing" state*/
  g_print ("Now playing: %s\n", argv[1]);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);


  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);


  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping playback\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Deleting pipeline\n");
  gst_object_unref (GST_OBJECT (pipeline));

  return 0;
}

