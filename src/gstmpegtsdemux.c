/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is Fluendo MPEG Demuxer plugin.
 *
 * The Initial Developer of the Original Code is Fluendo, S.A.
 * Portions created by Fluendo, S.A. are Copyright (C) 2005, 2006, 2007, 2008, 2009
 * Fluendo, S.A. All Rights Reserved.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>

#ifdef USE_LIBOIL
#include <liboil/liboil.h>
#endif

#include "gstmpegdefs.h"
#include "gstmpegtsdemux.h"
#include "flutspatinfo.h"
#include "flutspmtinfo.h"

GST_DEBUG_CATEGORY_STATIC (gstflutsdemux_debug);
#define GST_CAT_DEFAULT (gstflutsdemux_debug)

/* elementfactory information */
#ifdef USE_LIBOIL
#define LONGNAME "MPEG Transport stream demuxer (liboil build)"
#else
#define LONGNAME "MPEG Transport stream demuxer"
#endif

#ifndef __always_inline
#if (__GNUC__ > 3) || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1)
#define __always_inline inline __attribute__((always_inline))
#else
#define __always_inline inline
#endif
#endif

#ifndef DISABLE_INLINE
#define FORCE_INLINE __always_inline
#else
#define FORCE_INLINE
#endif

/* GStreamer compatibility */
#ifndef GST_TAG_CONTAINER_FORMAT
#define GST_TAG_CONTAINER_FORMAT "container-format"
#endif

/* MPEG2Demux signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

#define DEFAULT_PROP_ES_PIDS              ""
#define DEFAULT_PROP_CHECK_CRC            TRUE
#define DEFAULT_PROP_FORCE_PCR_SEGMENT    FALSE
#define DEFAULT_PROP_PROGRAM_NUMBER       -1
#define DEFAULT_PCR_DIFFERENCE            5000
#define DEFAULT_PTS_OFFSET                0

/* latency in mseconds */
#define TS_LATENCY 700

enum
{
  PROP_0,
  PROP_ES_PIDS,
  PROP_CHECK_CRC,
  PROP_PROGRAM_NUMBER,
  PROP_PAT_INFO,
  PROP_PMT_INFO,
  PROP_MAX_PES_BUFFER_SIZE,
  PROP_FORCE_PCR_SEGMENT,
  PROP_PCR_DIFFERENCE,
  PROP_PTS_OFFSET
};

#define GSTTIME_TO_BYTES(time) \
  ((time != -1) ? gst_util_uint64_scale (MAX(0,(gint64) ((time))), \
  demux->bitrate, GST_SECOND) : -1)
#define BYTES_TO_GSTTIME(bytes) \
  ((bytes != -1) ? (gst_util_uint64_scale (bytes, GST_SECOND, \
  demux->bitrate)) : -1)

#define VIDEO_CAPS \
  GST_STATIC_CAPS (\
    "video/mpeg, " \
      "mpegversion = (int) { 1, 2, 4 }, " \
      "systemstream = (boolean) FALSE; " \
    "video/x-h264, stream-format= (string) byte-stream;" \
    "video/x-dirac;" \
    "video/x-wmv," \
      "wmvversion = (int) 3, " \
      "format = (fourcc) WVC1" \
  )

#define AUDIO_CAPS \
  GST_STATIC_CAPS ( \
    "audio/mpeg, " \
      "mpegversion = (int) { 1, 2, 4 };" \
    "audio/x-lpcm, " \
      "width = (int) { 16, 20, 24 }, " \
      "rate = (int) { 48000, 96000 }, " \
      "channels = (int) [ 1, 8 ], " \
      "dynamic_range = (int) [ 0, 255 ], " \
      "emphasis = (boolean) { FALSE, TRUE }, " \
      "mute = (boolean) { FALSE, TRUE }; " \
    "audio/x-ac3; audio/x-eac3;" \
    "audio/x-dts;" \
    "audio/x-private-ts-lpcm;" \
    "audio/x-true-hd" \
  )

/* Can also use the subpicture pads for text subtitles? */
#define SUBPICTURE_CAPS \
    GST_STATIC_CAPS ("subpicture/x-pgs; video/x-dvd-subpicture")

#define MPEG4SL_CAPS \
    GST_STATIC_CAPS ("video/mpeg4sl")

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/mpegts")
    );

static GstStaticPadTemplate video_template =
GST_STATIC_PAD_TEMPLATE ("video_%04x",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    VIDEO_CAPS);

static GstStaticPadTemplate audio_template =
GST_STATIC_PAD_TEMPLATE ("audio_%04x",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    AUDIO_CAPS);

static GstStaticPadTemplate subpicture_template =
GST_STATIC_PAD_TEMPLATE ("subpicture_%04x",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    SUBPICTURE_CAPS);

static GstStaticPadTemplate private_template =
GST_STATIC_PAD_TEMPLATE ("private_%04x",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate mpeg4sl_template =
GST_STATIC_PAD_TEMPLATE ("mpeg4sl_%04x",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    MPEG4SL_CAPS);

static void gst_fluts_demux_base_init (GstFluTSDemuxClass * klass);
static void gst_fluts_demux_class_init (GstFluTSDemuxClass * klass);
static void gst_fluts_demux_init (GstFluTSDemux * demux);
static void gst_fluts_demux_finalize (GstFluTSDemux * demux);
static void gst_fluts_demux_reset (GstFluTSDemux * demux);
static void gst_fluts_demux_flush (GstFluTSDemux * demux, gboolean discard);

//static void gst_fluts_demux_remove_pads (GstFluTSDemux * demux);
static void gst_fluts_demux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_fluts_demux_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_fluts_demux_is_PMT (GstFluTSDemux * demux, guint16 PID);

static gboolean gst_fluts_demux_sink_event (GstPad * pad, GstEvent * event);
static gboolean gst_fluts_demux_src_event (GstPad * pad, GstEvent * event);
static GstFlowReturn gst_fluts_demux_chain (GstPad * pad, GstBuffer * buffer);
static gboolean gst_fluts_demux_sink_setcaps (GstPad * pad, GstCaps * caps);
static GstPadLinkReturn gst_fluts_demux_sink_linked (GstPad * pad, GstPad * peer);

#ifdef HAVE_LATENCY
static GstClock *gst_fluts_demux_provide_clock (GstElement * element);
#endif
static gboolean gst_fluts_demux_src_pad_query (GstPad * pad, GstQuery * query);
static const GstQueryType *gst_fluts_demux_src_pad_query_type (GstPad * pad);

static GstStateChangeReturn gst_fluts_demux_change_state (GstElement * element,
    GstStateChange transition);

static FluTsPmtInfo *fluts_demux_build_pmt_info (GstFluTSDemux * demux,
    guint16 pmt_pid);

static GstElementClass *parent_class = NULL;

static void gst_fluts_demux_refresh_es_state (GstFluTSDemux * demux);
/*static guint gst_fluts_demux_signals[LAST_SIGNAL] = { 0 };*/

GType
gst_fluts_demux_get_type (void)
{
  static GType fluts_demux_type = 0;

  if (G_UNLIKELY (!fluts_demux_type)) {
    static const GTypeInfo fluts_demux_info = {
      sizeof (GstFluTSDemuxClass),
      (GBaseInitFunc) gst_fluts_demux_base_init,
      NULL,
      (GClassInitFunc) gst_fluts_demux_class_init,
      NULL,
      NULL,
      sizeof (GstFluTSDemux),
      0,
      (GInstanceInitFunc) gst_fluts_demux_init,
    };

    fluts_demux_type =
        g_type_register_static (GST_TYPE_ELEMENT, "GstFluTSDemux",
        &fluts_demux_info, 0);

    gst_mpegtsdesc_init_debug ();

    GST_DEBUG_CATEGORY_INIT (gstflutsdemux_debug, "flutsdemux", 0,
        "MPEG program stream demultiplexer element");
  }

  return fluts_demux_type;
}

static void
gst_fluts_demux_base_init (GstFluTSDemuxClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  klass->sink_template = gst_static_pad_template_get (&sink_template);
  klass->video_template = gst_static_pad_template_get (&video_template);
  klass->audio_template = gst_static_pad_template_get (&audio_template);
  klass->subpicture_template =
      gst_static_pad_template_get (&subpicture_template);
  klass->private_template = gst_static_pad_template_get (&private_template);
  klass->mpeg4sl_template = gst_static_pad_template_get (&mpeg4sl_template);

  gst_element_class_add_pad_template (element_class, klass->video_template);
  gst_element_class_add_pad_template (element_class, klass->audio_template);
  gst_element_class_add_pad_template (element_class,
      klass->subpicture_template);
  gst_element_class_add_pad_template (element_class, klass->private_template);
  gst_element_class_add_pad_template (element_class, klass->mpeg4sl_template);
  gst_element_class_add_pad_template (element_class, klass->sink_template);

  gst_element_class_set_details_simple (element_class, LONGNAME,
    "Codec/Demuxer", "Demultiplexes MPEG2 Transport Streams",
    "Wim Taymans <wim@fluendo.com>");
}

static void
gst_fluts_demux_class_init (GstFluTSDemuxClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  parent_class = g_type_class_peek_parent (klass);

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->finalize = (GObjectFinalizeFunc) gst_fluts_demux_finalize;
  gobject_class->set_property = gst_fluts_demux_set_property;
  gobject_class->get_property = gst_fluts_demux_get_property;

  g_object_class_install_property (gobject_class, PROP_ES_PIDS,
      g_param_spec_string ("es-pids",
          "Colon separated list of PIDs containing Elementary Streams",
          "PIDs to treat as Elementary Streams in the absence of a PMT, "
          "eg 0x10:0x11:0x20", DEFAULT_PROP_ES_PIDS, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_CHECK_CRC,
      g_param_spec_boolean ("check-crc", "Check CRC",
          "Enable CRC checking", DEFAULT_PROP_CHECK_CRC, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PROGRAM_NUMBER,
      g_param_spec_int ("program-number", "Program Number",
          "Program number to demux for (-1 to ignore)", -1, G_MAXINT,
          DEFAULT_PROP_PROGRAM_NUMBER, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PAT_INFO,
      g_param_spec_value_array ("pat-info",
          "GValueArray containing GObjects with properties",
          "Array of GObjects containing information from the TS PAT "
          "about all programs listed in the current Program Association "
          "Table (PAT)",
          g_param_spec_object ("flu-pat-streaminfo", "FluPATStreamInfo",
              "Fluendo TS Demuxer PAT Stream info object",
              FLUTS_TYPE_PAT_INFO, G_PARAM_READABLE), G_PARAM_READABLE));

  g_object_class_install_property (gobject_class, PROP_PMT_INFO,
      g_param_spec_object ("pmt-info",
          "Information about the current program",
          "GObject with properties containing information from the TS PMT "
          "about the currently selected program and its streams",
          FLUTS_TYPE_PMT_INFO, G_PARAM_READABLE));

  g_object_class_install_property (gobject_class, PROP_MAX_PES_BUFFER_SIZE,
      g_param_spec_int ("max-pes-buffer-size", "Max PES buffer size",
          "max buffering size in bytes",
          2 * FLUTS_MIN_PES_BUFFER_SIZE, FLUTS_MAX_PES_BUFFER_SIZE,
          FLUTS_MAX_PES_BUFFER_SIZE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_FORCE_PCR_SEGMENT,
      g_param_spec_boolean ("force-pcr-segment", "Force PCR segment",
          "Produce data when the initial PCR sample is found",
          DEFAULT_PROP_FORCE_PCR_SEGMENT, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PCR_DIFFERENCE,
      g_param_spec_int ("pcr-difference", "PCR difference in mseconds",
          "PCR difference in seconds for bitrate calculation",
          0, G_MAXINT, DEFAULT_PCR_DIFFERENCE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_PTS_OFFSET,
      g_param_spec_uint64 ("pts-offset", "Initial PTS offset for timestamps",
          "Initial PTS offset for timestamps",
          0, G_MAXUINT64, DEFAULT_PTS_OFFSET, G_PARAM_READWRITE));

  gstelement_class->change_state = gst_fluts_demux_change_state;
#ifdef HAVE_LATENCY
  gstelement_class->provide_clock = gst_fluts_demux_provide_clock;
#endif
}

static void
gst_fluts_demux_init (GstFluTSDemux * demux)
{
  GstFluTSDemuxClass *klass = GST_FLUTS_DEMUX_GET_CLASS (demux);

  demux->streams = g_malloc0 (sizeof (GstFluTSStream *) * (FLUTS_MAX_PID + 1));
  demux->sinkpad = gst_pad_new_from_template (klass->sink_template, "sink");
  gst_pad_set_chain_function (demux->sinkpad, gst_fluts_demux_chain);
  gst_pad_set_event_function (demux->sinkpad, gst_fluts_demux_sink_event);
  gst_pad_set_setcaps_function (demux->sinkpad, gst_fluts_demux_sink_setcaps);
  gst_pad_set_link_function (demux->sinkpad, gst_fluts_demux_sink_linked);
  gst_element_add_pad (GST_ELEMENT (demux), demux->sinkpad);

  demux->elementary_pids = NULL;
  demux->nb_elementary_pids = 0;
  demux->check_crc = DEFAULT_PROP_CHECK_CRC;
  demux->force_pcr_segment = DEFAULT_PROP_FORCE_PCR_SEGMENT;
  demux->program_number = DEFAULT_PROP_PROGRAM_NUMBER;
  demux->max_pes_buffer_size = FLUTS_MAX_PES_BUFFER_SIZE;
  demux->pcr_difference = DEFAULT_PCR_DIFFERENCE;
  demux->pts_offset = DEFAULT_PTS_OFFSET;
  gst_fluts_demux_reset (demux);
#ifdef USE_LIBOIL
  oil_init ();
#endif
}

static void
gst_fluts_demux_finalize (GstFluTSDemux * demux)
{
  gst_fluts_demux_reset (demux);
  g_free (demux->streams);

  G_OBJECT_CLASS (parent_class)->finalize (G_OBJECT (demux));
}

static void
gst_fluts_demux_reset (GstFluTSDemux * demux)
{
  /* Clean up the streams and pads we allocated */
  gint i;

  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];

    if (stream != NULL) {
      if (stream->pad)
        gst_element_remove_pad (GST_ELEMENT_CAST (demux), stream->pad);
      if (stream->ES_info)
        gst_mpeg_descriptor_free (stream->ES_info);

      if (stream->PMT.entries)
        g_array_free (stream->PMT.entries, TRUE);
      if (stream->PMT.program_info)
        gst_mpeg_descriptor_free (stream->PMT.program_info);

      if (stream->PAT.entries)
        g_array_free (stream->PAT.entries, TRUE);

      switch (stream->PID_type) {
        case PID_TYPE_ELEMENTARY:
          gst_pes_filter_uninit (&stream->filter);
          break;
        case PID_TYPE_PROGRAM_ASSOCIATION:
        case PID_TYPE_CONDITIONAL_ACCESS:
        case PID_TYPE_PROGRAM_MAP:
        case PID_TYPE_PRIVATE_SECTION:
        case PID_TYPE_MPEG4_SECTION:
          gst_section_filter_uninit (&stream->section_filter);
          break;
      }
      if (stream->pes_buffer) {
        gst_buffer_unref (stream->pes_buffer);
        stream->pes_buffer = NULL;
      }
      if (stream->caps) {
        gst_caps_unref (stream->caps);
        stream->caps = NULL;
      }
      if (stream->name) {
        g_free (stream->name);
        stream->name = NULL;
      }
      g_free (stream);
      demux->streams[i] = NULL;
    }
  }

  demux->packetsize = 0;
  if (demux->sync_lut) {
    g_free (demux->sync_lut);
  }
  demux->sync_lut = NULL;
  demux->sync_lut_len = 0;
  demux->first_sync_scan = TRUE;
  demux->bitrate = -1;
  demux->num_packets = 0;
  demux->pcr[0] = -1;
  demux->pcr[1] = -1;
  demux->cache_duration = GST_CLOCK_TIME_NONE;
  demux->base_pts = GST_CLOCK_TIME_NONE;
  demux->requested_position = 0;
  demux->requested_rate = 1.0;
  demux->need_no_more_pads = FALSE;
  demux->pushing = FALSE;

  if (demux->new_seg_event) {
    gst_event_unref (demux->new_seg_event);
    demux->new_seg_event = NULL;
  }
#ifdef HAVE_LATENCY
  if (demux->clock) {
    g_object_unref (demux->clock);
    demux->clock = NULL;
  }
#endif
}

#if 0
static void
gst_fluts_demux_remove_pads (GstFluTSDemux * demux)
{
  /* remove pads we added in preparation for adding new ones */
  /* FIXME: instead of walking all streams, we should retain a list only
   * of streams that have added pads */
  gint i;

  if (demux->need_no_more_pads) {
    gst_element_no_more_pads ((GstElement *) demux);
    demux->need_no_more_pads = FALSE;
  }

  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];

    if (stream != NULL) {

      if (GST_IS_PAD (stream->pad)) {
        gst_pad_push_event (stream->pad, gst_event_new_eos ());
        gst_element_remove_pad (GST_ELEMENT_CAST (demux), stream->pad);
      }
      stream->pad = NULL;

      if (stream->PID_type == PID_TYPE_ELEMENTARY)
        gst_pes_filter_drain (&stream->filter);
    }
  }
}
#endif


static guint32 crc_tab[256] = {
  0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
  0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
  0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
  0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
  0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
  0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
  0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
  0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
  0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
  0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
  0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
  0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
  0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
  0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
  0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
  0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
  0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
  0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
  0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
  0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
  0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
  0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
  0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
  0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
  0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
  0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
  0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
  0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
  0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
  0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
  0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
  0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
  0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
  0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
  0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
  0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
  0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
  0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
  0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
  0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
  0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
  0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
  0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};

/* This function fills the value of negotiated packetsize at sinkpad */
static gboolean
gst_fluts_demux_sink_setcaps (GstPad * pad, GstCaps * caps)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  GstStructure *structure = NULL;

  structure = gst_caps_get_structure (caps, 0);

  GST_DEBUG_OBJECT (demux, "setcaps called with %" GST_PTR_FORMAT, caps);

  if (!gst_structure_get_int (structure, "packetsize", &demux->packetsize)) {
    GST_DEBUG_OBJECT (demux, "packetsize parameter not found in sink caps");
  }

  gst_object_unref (demux);
  return TRUE;
}

/* This function gets called when someone tries to link us on our sink pad */
static inline GstPadLinkReturn
gst_fluts_demux_handle_link (GstPad * pad, GstFluTSDemux * demux, GstPad * peer)
{
  GstPadLinkReturn ret = GST_PAD_LINK_OK;
  GstElement * parent = NULL, * grandparent = NULL;
  GstElementFactory * factory = NULL;

  /* We want to know if we are being used by playbin2. Indeed there are some
   * features we might want to disable. */
  parent = GST_ELEMENT (gst_element_get_parent (demux));
  grandparent = GST_ELEMENT (gst_element_get_parent (parent));
  while (grandparent) {
    /* Loose our reference to the current parent */
    gst_object_unref (parent);
    /* Move on to the higher step */
    parent = grandparent;
    /* Get the next grandparent */
    grandparent = GST_ELEMENT (gst_element_get_parent (parent));
  }
  
  /* Parent now points to the topmost element in the hierarchy */
  factory = gst_element_get_factory (parent);
  demux->playbin2 = FALSE;

  if (factory) {
#if GST_CHECK_VERSION (1,0,0)
    GST_DEBUG_OBJECT (demux, "topmost element factory name is %s",
        gst_plugin_feature_get_name (factory));
    
    if (!g_ascii_strcasecmp ("playbin", gst_plugin_feature_get_name (factory))) {
      demux->playbin2 = TRUE;
    }
#else
    GST_DEBUG_OBJECT (demux, "topmost element factory name is %s",
        GST_PLUGIN_FEATURE_NAME (factory));
    
    if (!g_ascii_strcasecmp ("playbin2", GST_PLUGIN_FEATURE_NAME (factory))) {
      demux->playbin2 = TRUE;
    }
#endif
  }

  gst_object_unref (parent);
  
  return ret;
}

static GstPadLinkReturn
gst_fluts_demux_sink_linked (GstPad * pad, GstPad * peer)
{
  GstFluTSDemux * demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  GstPadLinkReturn ret = gst_fluts_demux_handle_link (pad, demux, peer);
  
  gst_object_unref (demux);
  
  return ret;
}

static FORCE_INLINE guint32
gst_fluts_demux_calc_crc32 (guint8 * data, guint datalen)
{
  gint i;
  guint32 crc = 0xffffffff;

  for (i = 0; i < datalen; i++) {
    crc = (crc << 8) ^ crc_tab[((crc >> 24) ^ *data++) & 0xff];
  }
  return crc;
}

static FORCE_INLINE gboolean
gst_fluts_is_dirac_stream (GstFluTSStream * stream)
{
  gboolean is_dirac = FALSE;

  if (stream->stream_type != ST_VIDEO_DIRAC)
    return FALSE;

  if (stream->ES_info != NULL) {
    guint8 *dirac_desc;

    /* Check for a Registration Descriptor to confirm this is dirac */
    dirac_desc = gst_mpeg_descriptor_find (stream->ES_info, DESC_REGISTRATION);
    if (dirac_desc != NULL && DESC_LENGTH (dirac_desc) >= 4) {
      if (DESC_REGISTRATION_format_identifier (dirac_desc) == 0x64726163) {     /* 'drac' in hex */
        is_dirac = TRUE;
      }
    } else {
      /* Check for old mapping as originally specified too */
      dirac_desc = gst_mpeg_descriptor_find (stream->ES_info,
          DESC_DIRAC_TC_PRIVATE);
      if (dirac_desc != NULL && DESC_LENGTH (dirac_desc) == 0)
        is_dirac = TRUE;
    }
  }

  return is_dirac;
}

static FORCE_INLINE gboolean
gst_fluts_stream_is_video (GstFluTSStream * stream)
{
  switch (stream->stream_type) {
    case ST_VIDEO_MPEG1:
    case ST_VIDEO_MPEG2:
    case ST_VIDEO_MPEG4:
    case ST_VIDEO_H264:
      return TRUE;
    case ST_VIDEO_DIRAC:
      return gst_fluts_is_dirac_stream (stream);
  }

  return FALSE;
}

static gboolean
gst_fluts_demux_is_reserved_PID (GstFluTSDemux * demux, guint16 PID)
{
  return (PID >= PID_RESERVED_FIRST) && (PID < PID_RESERVED_LAST);
}

/* This function assumes that provided PID never will be greater than
 * FLUTS_MAX_PID (13 bits), this is currently guaranteed as everywhere in
 * the code recovered PID at maximum is 13 bits long.
 */
static FORCE_INLINE GstFluTSStream *
gst_fluts_demux_get_stream_for_PID (GstFluTSDemux * demux, guint16 PID)
{
  GstFluTSStream *stream = NULL;

  stream = demux->streams[PID];

  if (G_UNLIKELY (stream == NULL)) {
    stream = g_new0 (GstFluTSStream, 1);

    stream->demux = demux;
    stream->PID = PID;
    stream->pad = NULL;
    stream->base_PCR = -1;
    stream->last_PCR = -1;
    stream->avg_PCR_difference = 0;
    stream->PMT.version_number = -1;
    stream->PAT.version_number = -1;
    stream->PMT_pid = FLUTS_MAX_PID + 1;
    stream->flags |= FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
    stream->activation = FLUTS_STREAM_NOT_ACTIVATED;
    stream->pes_buffer_in_sync = FALSE;
    stream->need_segment = TRUE;
    stream->need_discont = TRUE;
    switch (PID) {
        /* check for fixed mapping */
      case PID_PROGRAM_ASSOCIATION_TABLE:
        stream->PID_type = PID_TYPE_PROGRAM_ASSOCIATION;
        /* initialise section filter */
        gst_section_filter_init (&stream->section_filter);
        break;
      case PID_CONDITIONAL_ACCESS_TABLE:
        stream->PID_type = PID_TYPE_CONDITIONAL_ACCESS;
        /* initialise section filter */
        gst_section_filter_init (&stream->section_filter);
        break;
      case PID_NULL_PACKET:
        stream->PID_type = PID_TYPE_NULL_PACKET;
        break;
      default:
        /* mark reserved PIDs */
        if (gst_fluts_demux_is_reserved_PID (demux, PID)) {
          stream->PID_type = PID_TYPE_RESERVED;
        } else {
          /* check if PMT found in PAT */
          if (gst_fluts_demux_is_PMT (demux, PID)) {
            stream->PID_type = PID_TYPE_PROGRAM_MAP;
            /* initialise section filter */
            gst_section_filter_init (&stream->section_filter);
          } else
            stream->PID_type = PID_TYPE_UNKNOWN;
        }
        break;
    }
    GST_DEBUG_OBJECT (demux, "creating stream %p for PID 0x%04x, PID_type %d",
        stream, PID, stream->PID_type);

    demux->streams[PID] = stream;
  }

  return stream;
}

static gboolean
gst_fluts_demux_fill_stream (GstFluTSStream * stream, guint8 id,
    guint8 stream_type)
{
  GstPadTemplate *template;
  gchar *name;
  GstFluTSDemuxClass *klass;
  GstFluTSDemux *demux;
  GstCaps *caps;
  gboolean is_hdmv = FALSE;
  GstFluTSStream *PMT_stream = NULL;
  GstMPEGDescriptor *program_info;

  PMT_stream = gst_fluts_demux_get_stream_for_PID (stream->demux,
      stream->PMT_pid);
  program_info = PMT_stream->PMT.program_info;

  if (program_info) {
    guint8 *desc = NULL;

    desc = gst_mpeg_descriptor_find (program_info, DESC_REGISTRATION);

    if (desc && DESC_REGISTRATION_format_identifier (desc) == DRF_ID_HDMV) {
      is_hdmv = TRUE;
    }
  }

  demux = stream->demux;
  klass = GST_FLUTS_DEMUX_GET_CLASS (demux);

  name = NULL;
  template = NULL;
  caps = NULL;
  stream->is_valid_for_segment = TRUE;

  switch (stream_type) {
    case ST_VIDEO_MPEG1:
    case ST_VIDEO_MPEG2:
      template = klass->video_template;
      name = g_strdup_printf ("video_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/mpeg",
          "mpegversion", G_TYPE_INT, stream_type == ST_VIDEO_MPEG1 ? 1 : 2,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;
    case ST_AUDIO_MPEG1:
    case ST_AUDIO_MPEG2:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 1, NULL);
      break;
    case ST_PRIVATE_DATA:
    {
      if (stream->ES_info) {
        guint8 *desc =
            gst_mpeg_descriptor_find (stream->ES_info, DESC_REGISTRATION);
        /* check if there is an AC3 descriptor associated with this stream
         * from the PMT */
        if (gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_AC3)) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-ac3", NULL); 
        } else if (gst_mpeg_descriptor_find (stream->ES_info,
                      DESC_DVB_ENHANCED_AC3)) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-eac3", NULL);
        } else if (gst_mpeg_descriptor_find (stream->ES_info,
                      DESC_DVB_DTS)) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-dts", NULL);
        } else if (gst_mpeg_descriptor_find (stream->ES_info,
                      DESC_DVB_TELETEXT)) {
          template = klass->private_template;
          name = g_strdup_printf ("private_%04x", stream->PID);
          caps = gst_caps_new_simple ("private/teletext", NULL);
          stream->is_valid_for_segment = FALSE;
        } else if (desc &&
              DESC_REGISTRATION_format_identifier (desc) == DRF_ID_DTS1) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-dts", NULL);
        } else if (desc &&
              DESC_REGISTRATION_format_identifier (desc) == DRF_ID_DTS2) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-dts", NULL);
        } else if (desc &&
              DESC_REGISTRATION_format_identifier (desc) == DRF_ID_DTS3) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-dts", NULL);
        } else if (desc &&
              DESC_REGISTRATION_format_identifier (desc) == DRF_ID_HEVC) {
          template = klass->video_template;
          name = g_strdup_printf ("video_%04x", stream->PID);
          caps = gst_caps_new_simple ("video/x-h265",
              "stream-format", G_TYPE_STRING, "byte-stream", NULL);
        } else if (gst_mpeg_descriptor_find (stream->ES_info,
			                      DESC_DVB_SUBTITLING)) {
          template = klass->subpicture_template;
          name = g_strdup_printf ("subpicture_%02x", stream->PID);
          caps = gst_caps_new_simple ("subpicture/x-dvb", NULL);
        }
      }
      
      /* Handle ECM/EMM streams */
      if (name == NULL || template == NULL || caps == NULL) {
        if (stream->flags & FLUTS_STREAM_FLAG_IS_ECM) {
          template = klass->private_template;
          name = g_strdup_printf ("private_%04x", stream->PID);
          caps = gst_caps_new_simple ("private/ecm", NULL);
        } else if (stream->flags & FLUTS_STREAM_FLAG_IS_EMM) {
          template = klass->private_template;
          name = g_strdup_printf ("private_%04x", stream->PID);
          caps = gst_caps_new_simple ("private/emm", NULL);
        }
        stream->is_valid_for_segment = FALSE;
      }
      break;
    }
    case ST_HDV_AUX_V:
      template = klass->private_template;
      name = g_strdup_printf ("private_%04x", stream->PID);
      caps = gst_caps_new_simple ("hdv/aux-v", NULL);
      break;
    case ST_HDV_AUX_A:
      template = klass->private_template;
      name = g_strdup_printf ("private_%04x", stream->PID);
      caps = gst_caps_new_simple ("hdv/aux-a", NULL);
      break;
    case ST_PRIVATE_SECTIONS:
    case ST_MHEG:
    case ST_DSMCC:
    case ST_MPEG4_SECTION:
      break;
    case ST_MPEG4_ES:
      template = klass->mpeg4sl_template;
      name = g_strdup_printf ("mpeg4sl_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/mpeg4sl", NULL);
      break;
    case ST_AUDIO_AAC_13818_7:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 2,
          "stream-format", G_TYPE_STRING, "adts", NULL);
      break;
    case ST_AUDIO_AAC_14496_3:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 4,
          "stream-format", G_TYPE_STRING, "latm", NULL);
      break;
    case ST_VIDEO_MPEG4:
      template = klass->video_template;
      name = g_strdup_printf ("video_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/mpeg",
          "mpegversion", G_TYPE_INT, 4,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;
    case ST_VIDEO_H264:
      template = klass->video_template;
      name = g_strdup_printf ("video_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/x-h264",
          "stream-format", G_TYPE_STRING, "byte-stream", NULL);
      break;
    case ST_VIDEO_HEVC:
      template = klass->video_template;
      name = g_strdup_printf ("video_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/x-h265",
          "stream-format", G_TYPE_STRING, "byte-stream", NULL);
      break;
    case ST_VIDEO_DIRAC:
      if (gst_fluts_is_dirac_stream (stream)) {
        template = klass->video_template;
        name = g_strdup_printf ("video_%04x", stream->PID);
        caps = gst_caps_new_simple ("video/x-dirac", NULL);
      }
      break;
    case ST_PRIVATE_EA:        /* Try to detect a VC1 stream */
    {
      guint8 *desc = NULL;

      if (stream->ES_info)
        desc = gst_mpeg_descriptor_find (stream->ES_info, DESC_REGISTRATION);
      if (!(desc && DESC_REGISTRATION_format_identifier (desc) == DRF_ID_VC1)) {
        GST_WARNING ("0xea private stream type found but no descriptor "
            "for VC1. Assuming plain VC1.");
      }
      template = klass->video_template;
      name = g_strdup_printf ("video_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/x-wmv",
          "wmvversion", G_TYPE_INT, 3,
          "format", GST_TYPE_FOURCC, GST_MAKE_FOURCC ('W', 'V', 'C', '1'),
          NULL);
      break;
    }
    case ST_BD_AUDIO_AC3:
    {
      guint8 *desc = NULL;

      if (is_hdmv) {
        if (stream->ES_info) {
          desc = gst_mpeg_descriptor_find (stream->ES_info,
              DESC_AC3_AUDIO_STREAM);
        } else {
          desc = NULL;
        }
        if (desc && DESC_AC_AUDIO_STREAM_bsid (desc) != 16) {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-ac3", NULL);
        }
        else {
          template = klass->audio_template;
          name = g_strdup_printf ("audio_%04x", stream->PID);
          caps = gst_caps_new_simple ("audio/x-eac3", NULL);
        }
      } else if (stream->ES_info && gst_mpeg_descriptor_find (stream->ES_info,
              DESC_DVB_ENHANCED_AC3)) {
        template = klass->audio_template;
        name = g_strdup_printf ("audio_%04x", stream->PID);
        caps = gst_caps_new_simple ("audio/x-eac3", NULL);
      } else {
        if (stream->ES_info &&
              !gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_AC3)) {
          GST_WARNING ("AC3 stream type found but no corresponding "
              "descriptor to differentiate between AC3 and EAC3. "
              "Assuming plain AC3.");
        }
        template = klass->audio_template;
        name = g_strdup_printf ("audio_%04x", stream->PID);
        caps = gst_caps_new_simple ("audio/x-ac3", NULL);
      }
      break;
    }
    case ST_BD_AUDIO_EAC3:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/x-eac3", NULL);
      break;
    case ST_BD_AUDIO_DTS:
    case ST_PS_AUDIO_DTS:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/x-dts", NULL);
      break;
    case ST_BD_AUDIO_AC3_TRUE_HD:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/x-true-hd", NULL);
      break;
    case ST_PS_AUDIO_LPCM:
      template = klass->audio_template;
      name = g_strdup_printf ("audio_%04x", stream->PID);
      caps = gst_caps_new_simple ("audio/x-lpcm", NULL);
      break;
    case ST_BD_AUDIO_LPCM:
      if (is_hdmv) {
        template = klass->audio_template;
        name = g_strdup_printf ("audio_%04x", stream->PID);
        caps = gst_caps_new_simple ("audio/x-private-ts-lpcm", NULL);
      }
      break;
    case ST_PS_DVD_SUBPICTURE:
      template = klass->subpicture_template;
      name = g_strdup_printf ("subpicture_%04x", stream->PID);
      caps = gst_caps_new_simple ("video/x-dvd-subpicture", NULL);
      break;
    case ST_BD_PGS_SUBPICTURE:
      template = klass->subpicture_template;
      name = g_strdup_printf ("subpicture_%04x", stream->PID);
      caps = gst_caps_new_simple ("subpicture/x-pgs", NULL);
      break;
    default:
      break;
  }
  if (name == NULL || template == NULL || caps == NULL)
    return FALSE;

  stream->stream_type = stream_type;
  stream->id = id;

  if (stream->caps)
    gst_caps_unref (stream->caps);
  stream->caps = caps;
  if (stream->name)
    g_free (stream->name);
  stream->name = name;
  stream->template = template;

  return TRUE;
}

static FORCE_INLINE gboolean
fluts_is_elem_pid (GstFluTSDemux * demux, guint16 PID)
{
  int i;

  /* check if it's in our partial ts pid list */
  for (i = 0; i < demux->nb_elementary_pids; i++) {
    if (demux->elementary_pids[i] == PID) {
      return TRUE;
    }
  }

  return FALSE;
}

static gboolean
gst_fluts_demux_setup_base_pts (GstFluTSDemux * demux, gint64 pts,
    GstFluTSStream * PTS_stream)
{
  GstFluTSStream *PCR_stream;
  GstFluTSStream *PMT_stream;
  guint64 base_PCR;

  /* for the reference start time we need to consult the PCR_PID of the
   * current PMT */
  if (demux->current_PMT == 0)
    goto no_pmt_stream;

  PMT_stream = demux->streams[demux->current_PMT];
  if (PMT_stream == NULL)
    goto no_pmt_stream;

  PCR_stream = demux->streams[PMT_stream->PMT.PCR_PID];
  if (PCR_stream == NULL)
    goto no_pcr_stream;

  if (demux->force_pcr_segment) {
    if (PCR_stream->last_PCR == -1) {
      goto no_time_reference;
    }
  } else {
    /* We can't do anything in that case */
    if (PCR_stream->base_PCR == -1 && PCR_stream->last_PCR == -1 && pts == -1) {
      goto no_time_reference;
    }
  }

  if (PCR_stream->base_PCR == -1 && PCR_stream->last_PCR == -1) {
    if (!PTS_stream->is_valid_for_segment)
	  goto unusable_pts_stream_type;

    GST_DEBUG_OBJECT (demux, "no base/last PCR, using PTS %" \
        G_GUINT64_FORMAT, pts);
    PCR_stream->base_PCR = pts;
  }
  else {
    if (PCR_stream->base_PCR == -1) {
      GST_DEBUG_OBJECT (demux, "no base PCR, using last PCR %" \
          G_GUINT64_FORMAT, PCR_stream->last_PCR);
      PCR_stream->base_PCR = PCR_stream->last_PCR;
    }
    else {
      GST_DEBUG_OBJECT (demux, "using base PCR %" G_GUINT64_FORMAT,
        PCR_stream->base_PCR);
    }
  }

  base_PCR = PCR_stream->base_PCR;

  if (!demux->force_pcr_segment && PTS_stream->is_valid_for_segment && pts < base_PCR) {
    /* In some clips the PTS are completely unrelated to PCR so if PTS is
     * inferior to the first PCR, we use it as the segment start */
    demux->base_pts = MPEGTIME_TO_GSTTIME (pts);
    GST_DEBUG_OBJECT (demux, "segment PTS (PTS based) to (%"
        G_GUINT64_FORMAT ")", pts);
  } else {
    demux->base_pts =  MPEGTIME_TO_GSTTIME (base_PCR);
    GST_DEBUG_OBJECT (demux, "segment PTS (PCR based) to (%"
        G_GUINT64_FORMAT ")", base_PCR);
  }

  if (demux->base_pts == GST_CLOCK_TIME_NONE)
    return FALSE;

  /* Offset the first PTS by the base time of our streams */
  demux->base_pts += PCR_stream->base_time;

  GST_DEBUG_OBJECT (demux, "base pts is now %" GST_TIME_FORMAT,
      GST_TIME_ARGS (demux->base_pts));

  return TRUE;

no_pmt_stream:
  {
    GST_DEBUG_OBJECT (demux, "no PMT stream found");
    return FALSE;
  }
no_pcr_stream:
  {
    GST_DEBUG_OBJECT (demux, "no PCR stream found");
    return FALSE;
  }
no_time_reference:
  {
    GST_DEBUG_OBJECT (demux, "no time reference (base_pcr/last_pcr/pts)");
    return FALSE;
  }
unusable_pts_stream_type:
  {
    GST_DEBUG_OBJECT (demux, "can't use the PTS from stream type 0x%02x",
        PTS_stream->stream_type);
    return FALSE;
  }
}

static gboolean
gst_fluts_demux_send_new_segment (GstFluTSDemux * demux,
    GstFluTSStream * stream, gint64 pts, GstClockTimeDiff base_time)
{
  GstClockTime time;

  /* base_pts needs to have been set up by a call to
   * gst_fluts_demux_setup_base_pts() before calling this function */
  if (demux->base_pts == GST_CLOCK_TIME_NONE)
    goto no_base_time;

  time = demux->base_pts;

  if (G_UNLIKELY (!demux->new_seg_event)) {
    if (demux->requested_rate > 0) {
      GST_DEBUG_OBJECT (demux, "segment PTS to time: %"
          GST_TIME_FORMAT " position %" GST_TIME_FORMAT,
          GST_TIME_ARGS (time), GST_TIME_ARGS (demux->requested_position));
      demux->new_seg_event = gst_event_new_new_segment (FALSE,
          demux->requested_rate, GST_FORMAT_TIME, time, -1,
          demux->requested_position);
    }
    else {
      GST_DEBUG_OBJECT (demux, "segment (reverse) PTS to time: %"
          GST_TIME_FORMAT " position %" GST_TIME_FORMAT,
          GST_TIME_ARGS (time), GST_TIME_ARGS (demux->requested_position));
      demux->new_seg_event = gst_event_new_new_segment (FALSE,
          demux->requested_rate, GST_FORMAT_TIME, 0, time,
          demux->requested_position);
    }
  }
  else {
    GST_DEBUG_OBJECT (demux, "pushing stored newsegment event");
  }
#ifdef HAVE_LATENCY
  if (demux->clock && demux->clock_base == GST_CLOCK_TIME_NONE) {
    demux->clock_base = gst_clock_get_time (demux->clock);
    gst_clock_set_calibration (demux->clock,
        gst_clock_get_internal_time (demux->clock), demux->clock_base, 1, 1);
  }
#endif

  /* Now push the newsegment event */
  gst_pad_push_event (stream->pad, gst_event_ref (demux->new_seg_event));

  return TRUE;

  /* ERRORS */
no_base_time:
  {
    /* check if it's in our partial ts pid list */
    if (fluts_is_elem_pid (demux, stream->PID)) {
      GST_DEBUG_OBJECT (demux, "Elementary PID, using pts %" G_GUINT64_FORMAT,
          pts);
      time = MPEGTIME_TO_GSTTIME (pts) + base_time;
      GST_DEBUG_OBJECT (demux, "segment PTS to (%" G_GUINT64_FORMAT ") time: %"
          G_GUINT64_FORMAT, pts, time);

      gst_pad_push_event (stream->pad,
          gst_event_new_new_segment (FALSE, 1.0, GST_FORMAT_TIME, time, -1,
              demux->requested_position));
      return TRUE;
    }
    else {
      GST_DEBUG_OBJECT (demux, "no base time to send a newsegment");
    }
  }
  return FALSE;
}

static void
gst_fluts_demux_send_tags_for_stream (GstFluTSDemux * demux,
    GstFluTSStream * stream)
{
  GstTagList *list = NULL;
  GstFluTSDemuxClass *klass = GST_FLUTS_DEMUX_GET_CLASS (demux);

  list = gst_tag_list_new ();

  gst_tag_register (GST_TAG_CONTAINER_FORMAT, GST_TAG_FLAG_ENCODED,
      G_TYPE_STRING, "container format", "container format", NULL);

  gst_tag_register (AVCOMPONENT_TAG_COMPONENT_TAG, GST_TAG_FLAG_META,
                    G_TYPE_UINT, "component tag", "component tag", NULL);

  gst_tag_register (AVCOMPONENT_TAG_PID, GST_TAG_FLAG_META,
        G_TYPE_UINT, "av pid", "AVComponent's pid", NULL);

  gst_tag_register (AVCOMPONENT_TAG_IS_ENCRYPTED, GST_TAG_FLAG_META,
        G_TYPE_BOOLEAN, "is encrypted", "If stream is encrypted (AVComponent spec)", NULL);

  gst_tag_register (AVCOMPONENT_TAG_AUDIO_DESCRIPTION, GST_TAG_FLAG_META,
        G_TYPE_BOOLEAN, "audio description", "(AVComponent spec)", NULL);

  gst_tag_register (AVCOMPONENT_TAG_HEARING_IMPAIRED, GST_TAG_FLAG_META,
                    G_TYPE_BOOLEAN, "hearing impaired",
                    "If stream is for hearing impaired (AVComponent spec)", NULL);

  gst_tag_register (AVCOMPONENT_TAG_ASPECT_RATIO, GST_TAG_FLAG_META,
                    GST_TYPE_FRACTION, "aspect ratio",
                    "display aspect ratio (AVComponent spec)", NULL);


  if (stream->codec) {
    gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                      GST_TAG_CODEC, stream->codec, NULL);
  }

  gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                    AVCOMPONENT_TAG_IS_ENCRYPTED, stream->is_encrypted, NULL);

  gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                    AVCOMPONENT_TAG_HEARING_IMPAIRED, stream->for_hearing_impaired, NULL);

  gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
            AVCOMPONENT_TAG_COMPONENT_TAG, stream->component_tag, NULL);

  if (stream->PMT_pid <= FLUTS_MAX_PID)
    gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                      AVCOMPONENT_TAG_PID, stream->PMT_pid, NULL);

  gst_tag_list_add (list, GST_TAG_MERGE_REPLACE, GST_TAG_CONTAINER_FORMAT,
      "MPEG-2 Transport Stream", NULL);


  if (stream->ES_info) {
    /* Preparing aspect ratio tag */
    if (stream->template == klass->video_template) {
      const guint8 *desc =
        gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_COMPONENT);
      if (desc) {
        gint i;
        struct { guint16 num, den; } asp = { 0 };
        const guint8
          sc = DESC_DVB_COMPONENT_stream_content(desc),
          ct = DESC_DVB_COMPONENT_type(desc);
        static const struct {
          guint8 sc, ct;
        } asp16x9[] = {
          { 0x1, 0x2 }, { 0x1, 0x6 }, { 0x1, 0x7 }, { 0x1, 0xA },
          { 0x1, 0xB }, { 0x1, 0xE }, { 0x1, 0xF }, { 0x5, 0x3 },
          { 0x5, 0x7 }, { 0x5, 0xB }, { 0x5, 0xF }, { 0x5, 0x80 },
          { 0x5, 0x81 }, { 0x5, 0x82 }, { 0x5, 0x83 }

        }, asp4x3[] = {
          { 0x1, 0x1 }, { 0x1, 0x5 }, { 0x1, 0x9 }, { 0x1, 0xD },
          { 0x5, 0x1 }, { 0x5, 0x5 }
        };

        for (i = 0; i < G_N_ELEMENTS (asp16x9); i++) {
          if (asp16x9[i].sc == sc && asp16x9[i].ct == ct) {
            asp.num =  16;
            asp.den =  9;
          }
        }

        if (!asp.num)
          for (i = 0; i < G_N_ELEMENTS (asp4x3); i++) {
            if (asp4x3[i].sc == sc && asp4x3[i].ct == ct) {
              asp.num = 4;
              asp.den = 3;
            }
          }

        if (asp.num)
          gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                            AVCOMPONENT_TAG_ASPECT_RATIO, asp.num, asp.den, NULL);
      }
    } else {
      /* Preparing language tag */
      gchar lang_code[4] = { 0 };
      gboolean lang_code_found = FALSE;
      if (stream->template == klass->audio_template) {
        {
          const guint8 *desc =
            gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_AC3);
          if (!desc)
            desc = gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_ENHANCED_AC3);  
          if (desc) {
            if ((desc[2] & 0x80) && (desc[3] & 0x38) == (1<<4)) {
              /* There is an ac-3_descriptor or an enhanced_ac-3_descriptor with a
                 component_type field with the service_type flags set to Visually Impaired. */
              gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                                AVCOMPONENT_TAG_AUDIO_DESCRIPTION, TRUE, NULL);
            }
          }
        }
      
        {
          const guint8 *iso639_languages =
            gst_mpeg_descriptor_find (stream->ES_info, DESC_ISO_639_LANGUAGE);
          if (iso639_languages) {
            if (DESC_ISO_639_LANGUAGE_codes_n (iso639_languages)) {
              gchar *language_n = (gchar *)
                DESC_ISO_639_LANGUAGE_language_code_nth (iso639_languages, 0);
              lang_code[0] = language_n[0];
              lang_code[1] = language_n[1];
              lang_code[2] = language_n[2];
              lang_code_found = TRUE;

              /* audio_type field */
              if (language_n[3] == 0x03) {
                /* There is an audio component with an ISO_639_language_descriptor in the 
                   PMT with the audio_type field set to 0x03 */
                gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                                  AVCOMPONENT_TAG_AUDIO_DESCRIPTION, TRUE, NULL);
              }
            }
          }
        }

        /* We're overwriting the language if supplementary audio descriprtor presents */
        {
          const guint8 *desc =
            gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_EXTENSION);

          /* supplementary audio descriptor has priority */
          if (desc && desc[2] == DESC_SUPPLEMENTARY_AUDIO) {
            if (((desc[3] & 0x7C) >> 2) == 0x1) {
              /* There is a supplementary_audio_descriptor with the 
                 editorial_classification field set to 0x01 */
              gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                                AVCOMPONENT_TAG_AUDIO_DESCRIPTION, TRUE, NULL);
            }

            if (desc[3] & 0x1) {
              lang_code[0] = desc[4];
              lang_code[1] = desc[5];
              lang_code[2] = desc[6];
              lang_code_found = TRUE;
            }
          }
        }
      } else {                    /* Subtitles */
        const guint8 *desc =
          gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_SUBTITLING);
        /* We take the first language code, it's an error actually,
           caused by architectural problem: we have only one tag for a stream */
        if (desc && desc[1] >= 6) {
          lang_code[0] = desc[2];
          lang_code[1] = desc[3];
          lang_code[2] = desc[4];
          lang_code_found = TRUE;
        }

        /* For teletext subtitles */
        if (!lang_code_found) {
          desc = gst_mpeg_descriptor_find (stream->ES_info, DESC_DVB_TELETEXT);
          if (desc && desc[1] >= 5) {
            lang_code[0] = desc[2];
            lang_code[1] = desc[3];
            lang_code[2] = desc[4];
            lang_code_found = TRUE;
          }
        }
      }

      if (lang_code_found)
        gst_tag_list_add (list, GST_TAG_MERGE_REPLACE,
                          GST_TAG_LANGUAGE_CODE, lang_code, NULL);
    }
  }

  GST_DEBUG_OBJECT (demux, "Sending tags %p for pad %s:%s",
      list, GST_DEBUG_PAD_NAME (stream->pad));
  gst_element_found_tags_for_pad (GST_ELEMENT (demux), stream->pad, list);
}

#ifndef GST_FLOW_IS_SUCCESS
#define GST_FLOW_IS_SUCCESS(ret) ((ret) >= GST_FLOW_OK)
#endif

static GstFlowReturn
gst_fluts_demux_combine_flows (GstFluTSDemux * demux, GstFluTSStream * stream,
    GstFlowReturn ret)
{
  gint i;

  /* store the value */
  stream->last_ret = ret;

  /* if it's success we can return the value right away */
  if (GST_FLOW_IS_SUCCESS (ret))
    goto done;

  /* any other error that is not-linked can be returned right
   * away */
  if (ret != GST_FLOW_NOT_LINKED)
    goto done;

  /* only return NOT_LINKED if all other pads returned NOT_LINKED */
  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    if (!(stream = demux->streams[i]))
      continue;

    /* some other return value (must be SUCCESS but we can return
     * other values as well) */
    ret = stream->last_ret;
    if (ret != GST_FLOW_NOT_LINKED)
      goto done;
  }
  /* if we get here, all other pads were unlinked and we return
   * NOT_LINKED then */
done:
  return ret;
}

static GstFlowReturn
gst_fluts_demux_data_cb (GstPESFilter * filter, gboolean first,
    GstBuffer * buffer, GstFluTSStream * stream)
{
  GstFluTSDemux *demux;
  GstFlowReturn ret;
  GstPad *srcpad;
  gint64 pts;
  GstClockTime time;
  GstFluTSStream *PMT_stream = NULL;
  GstFluTSStream *PCR_stream = NULL;
  GstClockTimeDiff base_time;

  demux = stream->demux;

  if (stream->PMT_pid <= FLUTS_MAX_PID) {
    PMT_stream = demux->streams[stream->PMT_pid];
    if (PMT_stream) {
      PCR_stream = demux->streams[PMT_stream->PMT.PCR_PID];
    }
  }

  /* each ES stream holds the PTS wrap around component of the base time. */
  base_time = stream->base_time;
  if (PMT_stream) {
    /* PMT stream holds the PCR related component of the base time. */
    base_time += PMT_stream->base_time;
  }

  GST_LOG_OBJECT (demux, "got data on PID 0x%04x", stream->PID);

  if (filter->pts != -1) {
    pts = filter->pts;

    time = MPEGTIME_TO_GSTTIME (pts) + base_time;

    /* In reverse trick mode we can also try to detect discontinuities 
     * using PTS values */
    if (demux->requested_rate < 0.0 &&
        (stream->last_time > time && stream->last_time - time > GST_SECOND / 2)) {
      GST_DEBUG_OBJECT (demux, "detected reverse playback discontinuity " \
          "using PTS values: last observed pts %" GST_TIME_FORMAT \
          " current pts %" GST_TIME_FORMAT,
          GST_TIME_ARGS (stream->last_time), GST_TIME_ARGS (time));
      stream->need_discont = TRUE;
    }

    /* PTS information is scarse and less reliable than PCR, sometimes PCR
     * and PTS values are vey close to each other, sometimes they aren't.
     * Both can rollover when reaching their max value or min value for rewind.
     * PCR rollover is detected when parsing the adaptation field and is the
     * main trigger for accumulating wrap arounds in the stream->base_time
     * member. PTS might rollover at a different time and this code is supposed
     * to handle those cases by compensating locally. In the rare case where
     * we don't have any PCR observations we could use the PTS rollover as
     * a trigger to accumulate the wrap around in the stream->base_time. */

    /* First: Check how this PTS compares to the previous one for that stream.
     * We consider that a 10 minute gap in either direction indicates an
     * anomaly. */
    if ((stream->last_time > 0 && stream->last_time < time &&
            time - stream->last_time > GST_SECOND * 60 * 10) ||
        (stream->last_time > time &&
            stream->last_time - time > GST_SECOND * 60 * 10)) {
      /* Second: Check to see if we're in middle of detecting a discont in PCR.
       * If we are we're not sure what timestamp the buffer should have, best
       * to drop. */
      if (PCR_stream && PCR_stream->discont_PCR) {
        GST_WARNING_OBJECT (demux, "middle of discont, dropping");
        goto bad_timestamp;
      }
      /* We need to differentiate reverse and forward playback */
      if (demux->requested_rate > 0.0) {
        /* Third: Now we can have 2 scenarios, either the PTS are rolling over
         * before PCR did or vice versa.
         * a) PTS rolled over before PCR. Meaning the PTS values are starting
         * back from zero and the base_time for that stream did not change.
         * Our calculated time is much lower than the previously observed one. */
        if (stream->last_time > 0 && time < stream->last_time &&
            stream->last_time - time > MPEGTIME_TO_GSTTIME (G_MAXUINT32)) {
          /* Simulate PCR rollover and run a sanity check. */
          if ((base_time + MAX_MPEG_PTS + MPEGTIME_TO_GSTTIME (pts)) >
                (stream->last_time + GST_SECOND * 60 * 10)) {
            GST_DEBUG_OBJECT (demux, "looks like we have a corrupt packet " \
                "because its pts is a lot lower than the previous pts but " \
                "not a wraparound");
            goto bad_timestamp;
          }
          /* If we have a PCR stream with some observations we will accomodate
           * for the soon to come rollover locally until it happens in the
           * base_time of the stream. */
          if (PCR_stream && PCR_stream->last_PCR > 0) {
            GST_DEBUG_OBJECT (demux, "timestamps wrapped before noticed in PCR");
            time = MPEGTIME_TO_GSTTIME (pts) + base_time + MAX_MPEG_PTS;
            stream->last_time = time;
          }
          /* In the rare case where we don't have PCR observations we can
           * write down the rollover in the stream's base_time here. */
          else {
            stream->base_time += MAX_MPEG_PTS;
            base_time += MAX_MPEG_PTS;
            time = MPEGTIME_TO_GSTTIME (pts) + base_time;
            GST_DEBUG_OBJECT (demux,
                "timestamps wrapped around, compensating with new base time: %"
                GST_TIME_FORMAT "last time: %" GST_TIME_FORMAT " time: %"
                GST_TIME_FORMAT, GST_TIME_ARGS (base_time),
                GST_TIME_ARGS (stream->last_time), GST_TIME_ARGS (time));
            stream->last_time = time;
          }
        }
        /* b) PCR rolled over before PTS. Meaning that the stream's base_time
         * has been modified but the PTS values are still evolving monotonically.
         * Our calculated time is much higher than the previously observed one.
         */
        else if (stream->last_time > 0 && time > stream->last_time &&
            time - stream->last_time > MPEGTIME_TO_GSTTIME (G_MAXUINT32) &&
            base_time > demux->pts_offset) {
          /* Simulate PCR rollover removal and run a sanity check. */
          if (time - MAX_MPEG_PTS + GST_SECOND * 60 * 10 < stream->last_time) {
            GST_DEBUG_OBJECT (demux,
                "looks like we have a corrupt packet because its pts is a lot higher than"
                " the previous pts but not because of a wraparound or pcr discont");
            goto bad_timestamp;
          }
          /* If the gap between the last observed time for this stream and
           * the current time (taking in account the PCR rollover) is in an
           * acceptable range, accomodate locally until PTS rolls over too. */
          if (ABS ((gint64)time - (gint64)MAX_MPEG_PTS - (gint64)stream->last_time) < GST_SECOND) {
            GST_DEBUG_OBJECT (demux,
                "timestamps wrapped around earlier but we have an out of pts: %"
                G_GUINT64_FORMAT ", as %" GST_TIME_FORMAT " translated to: %"
                GST_TIME_FORMAT " and last_time of %" GST_TIME_FORMAT, pts,
                GST_TIME_ARGS (time),
                GST_TIME_ARGS (time - MAX_MPEG_PTS),
                GST_TIME_ARGS (stream->last_time));
            time -= MAX_MPEG_PTS;
          }
          else {
            GST_DEBUG_OBJECT (demux,
                "timestamp may have wrapped around recently but not sure and pts"
                " is very different, dropping it timestamp of this packet: %"
                GST_TIME_FORMAT " compared to last timestamp: %" GST_TIME_FORMAT,
                GST_TIME_ARGS (time - MAX_MPEG_PTS),
                GST_TIME_ARGS (stream->last_time));
            goto bad_timestamp;
          }
        }
        else {
          /* we must have a corrupt packet */
          GST_WARNING_OBJECT (demux, "looks like we have a corrupt packet " \
              "because its timestamp is buggered: %" GST_TIME_FORMAT \
              " compared to" " last timestamp: %" GST_TIME_FORMAT,
              GST_TIME_ARGS (time), GST_TIME_ARGS (stream->last_time));
          goto bad_timestamp;
        }
      }
      else {
        /* Third: Now we can have 2 scenarios, either the PTS are rolling over
         * before PCR did or vice versa.
         * a) PTS rolled over before PCR. Meaning the PTS values are starting
         * back from the max value and the base_time for that stream did not
         * change.
         * Our calculated time is much higher than the previously observed one. */
        if (stream->last_time > 0 && time > stream->last_time &&
            time - stream->last_time > MPEGTIME_TO_GSTTIME (G_MAXUINT32) &&
            base_time > 0) {
          /* Simulate PCR rollover and run a sanity check. */
          if ((base_time - MAX_MPEG_PTS + MPEGTIME_TO_GSTTIME (pts)) <
                (stream->last_time - GST_SECOND * 60 * 10)) {
            GST_DEBUG_OBJECT (demux, "looks like we have a corrupt packet " \
                "because its pts is a lot lower than the previous pts but " \
                "not a wraparound");
            goto bad_timestamp;
          }
          /* If we have a PCR stream with some observations we will accomodate
           * for the soon to come rollover locally until it happens in the
           * base_time of the stream. */
          if (PCR_stream && PCR_stream->last_PCR > 0) {
            GST_DEBUG_OBJECT (demux, "timestamps wrapped before noticed in " \
                "PCR (reverse)");
            time = MPEGTIME_TO_GSTTIME (pts) + base_time - MAX_MPEG_PTS;
            stream->last_time = time;
          }
          /* In the rare case where we don't have PCR observations we can
           * write down the rollover in the stream's base_time here. */
          else {
            stream->base_time -= MAX_MPEG_PTS;
            base_time -= MAX_MPEG_PTS;
            time = MPEGTIME_TO_GSTTIME (pts) + base_time;
            GST_DEBUG_OBJECT (demux,
                "timestamps wrapped around, compensating with new base time: %"
                GST_TIME_FORMAT "last time: %" GST_TIME_FORMAT " time: %"
                GST_TIME_FORMAT " (reverse)", GST_TIME_ARGS (base_time),
                GST_TIME_ARGS (stream->last_time), GST_TIME_ARGS (time));
            stream->last_time = time;
          }
        }
        /* b) PCR rolled over before PTS. Meaning that the stream's base_time
         * has been modified but the PTS values are still evolving
         * monotonically towards zero.
         * Our calculated time is much lower than the previously observed one.
         */
        else if (stream->last_time > 0 && time < stream->last_time &&
            stream->last_time - time > MPEGTIME_TO_GSTTIME (G_MAXUINT32)) {
          /* Simulate PCR rollover removal and run a sanity check. */
          if (time + MAX_MPEG_PTS - GST_SECOND * 60 * 10 < stream->last_time) {
            GST_DEBUG_OBJECT (demux,
                "looks like we have a corrupt packet because its pts is a lot higher than"
                " the previous pts but not because of a wraparound or pcr discont");
            goto bad_timestamp;
          }
          /* If the gap between the last observed time for this stream and
           * the current time (taking in account the PCR rollover) is in an
           * acceptable range, accomodate locally until PTS rolls over too. */
          if (ABS ((gint64)stream->last_time - ((gint64)time + (gint64)MAX_MPEG_PTS)) < GST_SECOND) {
            GST_DEBUG_OBJECT (demux,
                "timestamps wrapped around earlier but we have an out of pts: %"
                G_GUINT64_FORMAT ", as %" GST_TIME_FORMAT " translated to: %"
                GST_TIME_FORMAT " and last_time of %" GST_TIME_FORMAT \
                " (reverse)", pts, GST_TIME_ARGS (time),
                GST_TIME_ARGS (time - MAX_MPEG_PTS),
                GST_TIME_ARGS (stream->last_time));
            time += MAX_MPEG_PTS;
          }
          else {
            GST_DEBUG_OBJECT (demux,
                "timestamp may have wrapped around recently but not sure and pts"
                " is very different, dropping it timestamp of this packet: %"
                GST_TIME_FORMAT " compared to last timestamp: %" GST_TIME_FORMAT,
                GST_TIME_ARGS (time - MAX_MPEG_PTS),
                GST_TIME_ARGS (stream->last_time));
            goto bad_timestamp;
          }
        }
        else {
          /* we must have a corrupt packet */
          GST_WARNING_OBJECT (demux, "looks like we have a corrupt packet because"
              " its timestamp is buggered timestamp: %" GST_TIME_FORMAT
              " compared to" " last timestamp: %" GST_TIME_FORMAT,
              GST_TIME_ARGS (time), GST_TIME_ARGS (stream->last_time));
          goto bad_timestamp;
        }
      }
    }
    else {
      /* Remember this time as the last valid time for that stream */
      stream->last_time = time;
    }
  }
  else {
    time = GST_CLOCK_TIME_NONE;
    pts = -1;
  }

  GST_DEBUG_OBJECT (demux, "setting PTS to (%" G_GUINT64_FORMAT ") time: %"
      GST_TIME_FORMAT " base_time: %" GST_TIME_FORMAT, pts,
      GST_TIME_ARGS (time), GST_TIME_ARGS (base_time));

  GST_BUFFER_TIMESTAMP (buffer) = time;

  if (stream->activation == FLUTS_STREAM_ACTIVATE_ON_DATA) {
    /* First candidate to type identification is what we got in the PMT */
    guint8 stream_type = stream->stream_type;
    if (!stream->caps &&
        !gst_fluts_demux_fill_stream (stream, filter->id, stream_type)) {
      /* if couldn't be identified, then assume it based on the PES start code,
       * needed for partial ts streams without PMT or unknown type declared
       * in the PMT like private use 0x80 */
      if ((filter->start_code & 0xFFFFFFF0) == PACKET_VIDEO_START_CODE) {
        /* it is mpeg2 video */
        stream_type = ST_VIDEO_MPEG2;
        stream->flags &= ~FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
        stream->flags |= FLUTS_STREAM_FLAG_IS_VIDEO;
        GST_DEBUG_OBJECT (demux, "Found stream 0x%04x without PMT with video "
            "start_code. Treating as video", stream->PID);
      } else if ((filter->start_code & 0xFFFFFFE0) == PACKET_AUDIO_START_CODE) {
        /* it is mpeg audio */
        stream_type = ST_AUDIO_MPEG2;
        stream->flags &= ~FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
        GST_DEBUG_OBJECT (demux, "Found stream 0x%04x without PMT with audio "
            "start_code. Treating as audio", stream->PID);
      } else if (filter->start_code == ID_ECM_STREAM) {
        stream_type = ST_PRIVATE_DATA;
        stream->flags &= ~FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
        stream->flags |= FLUTS_STREAM_FLAG_IS_ECM;
        GST_DEBUG_OBJECT (demux, "Found stream 0x%04x without PMT with ECM "
            "start_code. Treating as private", stream->PID);
      } else if (filter->start_code == ID_EMM_STREAM) {
        stream_type = ST_PRIVATE_DATA;
        stream->flags &= ~FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
        stream->flags |= FLUTS_STREAM_FLAG_IS_EMM;
        GST_DEBUG_OBJECT (demux, "Found stream 0x%04x without PMT with EMM "
            "start_code. Treating as private", stream->PID);
      } else {
        GST_LOG_OBJECT (demux, "Stream start code on pid 0x%04x is: 0x%x",
            stream->PID, filter->start_code);
      }
      if (!gst_fluts_demux_fill_stream (stream, filter->id, stream_type)) {
        goto unknown_type;
      }
    }

    GST_DEBUG_OBJECT (demux, "creating new pad %s", stream->name);
    stream->pad = gst_pad_new_from_template (stream->template, stream->name);
    gst_pad_use_fixed_caps (stream->pad);
    gst_pad_set_caps (stream->pad, stream->caps);

    gst_pad_set_query_function (stream->pad,
        GST_DEBUG_FUNCPTR (gst_fluts_demux_src_pad_query));
    gst_pad_set_query_type_function (stream->pad,
        GST_DEBUG_FUNCPTR (gst_fluts_demux_src_pad_query_type));
    gst_pad_set_event_function (stream->pad,
        GST_DEBUG_FUNCPTR (gst_fluts_demux_src_event));

    GST_DEBUG_OBJECT (demux,
        "New stream 0x%04x of type 0x%02x with caps %" GST_PTR_FORMAT,
        stream->PID, stream->stream_type, GST_PAD_CAPS (stream->pad));

    /* activate and add */
    gst_pad_set_active (stream->pad, TRUE);
    gst_element_add_pad (GST_ELEMENT_CAST (demux), stream->pad);
    demux->need_no_more_pads = TRUE;

    stream->need_discont = TRUE;
    stream->activation = FLUTS_STREAM_ACTIVATED;

    /* send tags */
    gst_fluts_demux_send_tags_for_stream (demux, stream);
  }

  if (stream->activation != FLUTS_STREAM_ACTIVATED) {
    goto inactive;
  }

  srcpad = stream->pad;

  if (G_UNLIKELY (stream->need_segment)) {
    /* To send a new segment, require either a valid base PCR, or a valid PTS
     * We don't use PTS from Private streams as they can be completely off with
     * the others. */
    if (!gst_fluts_demux_setup_base_pts (demux, pts, stream))
      goto bad_timestamp;

    /* send new_segment */
    gst_fluts_demux_send_new_segment (demux, stream, pts, base_time);
    stream->need_segment = FALSE;
  }

  if (stream->need_discont) {
    GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_DISCONT);
    stream->need_discont = FALSE;
  }
  else {
    GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_DELTA_UNIT);
  }

  GST_DEBUG_OBJECT (srcpad, "pushing buffer with PTS %" GST_TIME_FORMAT \
      " size %u flags %x", GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer)),
      GST_BUFFER_SIZE (buffer), GST_BUFFER_FLAGS(buffer));
  gst_buffer_set_caps (buffer, GST_PAD_CAPS (srcpad));

  /* Put the payload_unit_start_indicator in the Buffer's flags */
  if (first)
    GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_MEDIA1);

  ret = gst_pad_push (srcpad, buffer);
  ret = gst_fluts_demux_combine_flows (demux, stream, ret);

  /* We've started pushing data */
  demux->pushing = TRUE;

  return ret;

  /* ERROR */
inactive:
  {
    GST_DEBUG_OBJECT (demux, "stream id 0x%02x, type 0x%02x is not activated",
        filter->id, stream->stream_type);
    gst_buffer_unref (buffer);
    return gst_fluts_demux_combine_flows (demux, stream, GST_FLOW_NOT_LINKED);
  }
unknown_type:
  {
    GST_DEBUG_OBJECT (demux, "got unknown stream id 0x%02x, type 0x%02x",
        filter->id, stream->stream_type);
    gst_buffer_unref (buffer);
    return gst_fluts_demux_combine_flows (demux, stream, GST_FLOW_NOT_LINKED);
  }
bad_timestamp:
  {
    gst_buffer_unref (buffer);
    return gst_fluts_demux_combine_flows (demux, stream, GST_FLOW_OK);
  }
}

static void
gst_fluts_demux_resync_cb (GstPESFilter * filter, GstFluTSStream * stream)
{
  /* does nothing for now */
}

/*
 * CA_section() {
 *   table_id                  8 uimsbf   == 0x01
 *   section_syntax_indicator  1 bslbf    == 1
 *   '0'                       1 bslbf    == 0
 *   reserved                  2 bslbf
 *   section_length           12 uimsbf   == 00xxxxx...
 *   reserved                 18 bslbf
 *   version_number            5 uimsbf
 *   current_next_indicator    1 bslbf
 *   section_number            8 uimsbf
 *   last_section_number       8 uimsbf
 *   for (i=0; i<N;i++) {
 *     descriptor()
 *   }
 *   CRC_32                   32 rpchof
 * }
 */
static FORCE_INLINE gboolean
gst_fluts_stream_parse_cat (GstFluTSStream * stream,
    guint8 * data, guint datalen)
{
  GstFluTSDemux *demux;
  guint32 CRC;
  GstFluTSCAT *CAT;
  guint version_number;
  guint8 current_next_indicator;

  demux = stream->demux;

  if (G_UNLIKELY (*data != 0x01))
    goto wrong_id;
  data += 1;
  if ((data[0] & 0xc0) != 0x80)
    goto wrong_sync;
  if ((data[0] & 0x0c) != 0x00)
    goto wrong_seclen;

  data += 2;

  if (demux->check_crc)
    if (G_UNLIKELY (gst_fluts_demux_calc_crc32 (data - 3, datalen) != 0))
      goto wrong_crc;

  GST_LOG_OBJECT (demux, "CAT section_length: %d", datalen - 3);

  /* check if version number changed */
  version_number = (data[2] & 0x3e) >> 1;
  GST_LOG_OBJECT (demux, "CAT version_number: %d", version_number);

  current_next_indicator = (data[2] & 0x01);
  GST_LOG_OBJECT (demux, "CAT current_next_indicator %d",
      current_next_indicator);
  if (current_next_indicator == 0)
    goto not_yet_applicable;

  CAT = &stream->CAT;

  if (version_number == CAT->version_number)
    goto same_version;

  CAT->version_number = version_number;
  CAT->current_next_indicator = current_next_indicator;

  CAT->section_number = *data++;
  GST_DEBUG_OBJECT (demux, "CAT section_number: %d", CAT->section_number);

  CAT->last_section_number = *data++;
  GST_DEBUG_OBJECT (demux, "CAT last_section_number: %d",
      CAT->last_section_number);

  stream->ES_info = gst_mpeg_descriptor_parse (data, datalen - 10);

  CRC = GST_READ_UINT32_BE (data);
  GST_DEBUG_OBJECT (demux, "CAT CRC: 0x%08x", CRC);

  return TRUE;

wrong_crc:
  {
    GST_DEBUG_OBJECT (demux, "wrong crc");
    return FALSE;
  }
same_version:
  {
    GST_DEBUG_OBJECT (demux, "same version as existing CAT");
    return TRUE;
  }
wrong_id:
  {
    GST_DEBUG_OBJECT (demux, "expected table_id == 0x1, got 0x%02x", data[0]);
    return FALSE;
  }
wrong_sync:
  {
    GST_DEBUG_OBJECT (demux, "expected sync 10, got %02x", data[0]);
    return FALSE;
  }
wrong_seclen:
  {
    GST_DEBUG_OBJECT (demux,
        "first two bits of section length must be 0, got %02x", data[0]);
    return FALSE;
  }
not_yet_applicable:
  {
    GST_DEBUG_OBJECT (demux, "Ignoring CAT with current_next_indicator = 0");
    return TRUE;
  }

  return TRUE;
}

static FORCE_INLINE GValueArray *
gst_fluts_build_descriptors_value_array (GstMPEGDescriptor *mpeg_desc)
{
  GString *desc;
  GValue value = { 0 };
  GValueArray *descriptors = g_value_array_new (mpeg_desc->n_desc);
  guint i;
  guint8 *data = mpeg_desc->data;

  for (i = 0; i < mpeg_desc->n_desc; i++) {
    guint8 length = DESC_LENGTH (data);

    /* include tag and length */
    desc = g_string_new_len ((gchar *) data, length + 2);
    data += length + 2;

    g_value_init (&value, G_TYPE_GSTRING);
    g_value_take_boxed (&value, desc);
    g_value_array_append (descriptors, &value);
    g_value_unset (&value);
  }

  return descriptors;
}

static FORCE_INLINE GstStructure *
gst_fluts_build_pat_structure (GstFluTSDemux *demux, GstFluTSPAT *pat)
{
  GstStructure *pat_struct = NULL;
  GValue entries = { 0 };
  GValue value = { 0 };
  GstStructure *entry = NULL;
  gchar *struct_name;
  gint i;

  g_return_val_if_fail (pat != NULL, NULL);

  GST_DEBUG_OBJECT (demux, "Building PAT structure");

  pat_struct = gst_structure_new ("pat",
      "transport-stream-id", G_TYPE_UINT, pat->transport_stream_id, NULL);
  g_value_init (&entries, GST_TYPE_LIST);
  for (i = 0; i < pat->entries->len; i++) {
    GstFluTSPATEntry *pmt_desc = &g_array_index (pat->entries, GstFluTSPATEntry,
        i);
    struct_name = g_strdup_printf ("program-%d", pmt_desc->program_number);
    entry = gst_structure_new (struct_name,
        "program-number", G_TYPE_UINT, pmt_desc->program_number,
        "pid", G_TYPE_UINT, pmt_desc->PID, NULL);
    g_free (struct_name);

    g_value_init (&value, GST_TYPE_STRUCTURE);
    g_value_take_boxed (&value, entry);
    gst_value_list_append_value (&entries, &value);
    g_value_unset (&value);
  }

  gst_structure_set_value (pat_struct, "programs", &entries);
  g_value_unset (&entries);

  GST_DEBUG_OBJECT (demux, "Created PAT structure: %" GST_PTR_FORMAT,
      pat_struct);

  return pat_struct;
}

static FORCE_INLINE GstStructure *
gst_fluts_build_pmt_structure (GstFluTSDemux *demux, GstFluTSPMT *pmt)
{
  GstStructure *pmt_struct = NULL;
  gint i;
  GValueArray *descriptors;
  GValue stream_value = { 0 };
  GValue programs = { 0 };
  GstStructure *stream_info = NULL;
  gchar *struct_name;

  g_return_val_if_fail (pmt != NULL, NULL);

  GST_DEBUG_OBJECT (demux, "Building PMT structure");

  struct_name = g_strdup ("pmt");
  pmt_struct = gst_structure_new (struct_name,
      "program-number", G_TYPE_UINT, pmt->program_number,
      "pcr-pid", G_TYPE_UINT, pmt->PCR_PID,
      "version-number", G_TYPE_UINT, pmt->version_number, NULL);
  g_free (struct_name);

  if (pmt->program_info_length) {
    descriptors = gst_fluts_build_descriptors_value_array (pmt->program_info);

    gst_structure_set (pmt_struct, "descriptors", G_TYPE_VALUE_ARRAY,
        descriptors, NULL);
    g_value_array_free (descriptors);
  }

  g_value_init (&programs, GST_TYPE_LIST);
  for (i = 0; i < pmt->entries->len; i++) {
    GstFluTSPMTEntry *cur_entry =
        &g_array_index (pmt->entries, GstFluTSPMTEntry, i);
    GstFluTSStream *ES_stream = gst_fluts_demux_get_stream_for_PID (demux,
        cur_entry->PID);
    if (!ES_stream)
      continue;

    struct_name = g_strdup_printf ("pid-%d", cur_entry->PID);
    stream_info = gst_structure_new (struct_name,
        "pid", G_TYPE_UINT, cur_entry->PID,
        "stream-type", G_TYPE_UINT, ES_stream->stream_type, NULL);
    g_free (struct_name);

    if (ES_stream->ES_info && ES_stream->ES_info->n_desc) {
      descriptors = gst_fluts_build_descriptors_value_array (ES_stream->ES_info);

      gst_structure_set (stream_info,
          "descriptors", G_TYPE_VALUE_ARRAY, descriptors, NULL);

      g_value_array_free (descriptors);
    }

    g_value_init (&stream_value, GST_TYPE_STRUCTURE);
    g_value_take_boxed (&stream_value, stream_info);
    gst_value_list_append_value (&programs, &stream_value);

    g_value_unset (&stream_value);
  }

  gst_structure_set_value (pmt_struct, "streams", &programs);
  g_value_unset (&programs);

  GST_DEBUG_OBJECT (demux, "Created PMT structure: %" GST_PTR_FORMAT,
      pmt_struct);

  return pmt_struct;
}

static void
gst_fluts_activate_pmt (GstFluTSDemux * demux, GstFluTSStream * stream)
{
  GST_DEBUG_OBJECT (demux, "activating PMT 0x%04x", stream->PID);

  /* gst_fluts_demux_remove_pads (demux); */

  demux->current_PMT = stream->PID;

  /* PMT has been updated, signal the change */
  if (demux->current_PMT == stream->PID)
    g_object_notify ((GObject *) (demux), "pmt-info");

  gst_fluts_demux_refresh_es_state (demux);
}

/*
 * TS_program_map_section() {
 *   table_id                          8 uimsbf   == 0x02
 *   section_syntax_indicator          1 bslbf    == 1
 *   '0'                               1 bslbf    == 0
 *   reserved                          2 bslbf
 *   section_length                   12 uimsbf   == 00xxxxx...
 *   program_number                   16 uimsbf
 *   reserved                          2 bslbf
 *   version_number                    5 uimsbf
 *   current_next_indicator            1 bslbf
 *   section_number                    8 uimsbf
 *   last_section_number               8 uimsbf
 *   reserved                          3 bslbf
 *   PCR_PID                          13 uimsbf
 *   reserved                          4 bslbf
 *   program_info_length              12 uimsbf   == 00xxxxx...
 *   for (i=0; i<N; i++) {
 *     descriptor()
 *   }
 *   for (i=0;i<N1;i++) {
 *     stream_type                     8 uimsbf
 *     reserved                        3 bslbf
 *     elementary_PID                 13 uimsnf
 *     reserved                        4 bslbf
 *     ES_info_length                 12 uimsbf   == 00xxxxx...
 *     for (i=0; i<N2; i++) {
 *       descriptor()
 *     }
 *   }
 *   CRC_32                           32 rpchof
 * }
 */
static FORCE_INLINE gboolean
gst_fluts_stream_parse_pmt (GstFluTSStream * stream,
    guint8 * data, guint datalen)
{
  GstFluTSDemux *demux;
  GstFluTSDemuxClass *klass;
  gint entries, nb_audios = 0, nb_videos = 0;
  guint32 CRC;
  GstFluTSPMT *PMT;
  guint version_number;
  guint8 current_next_indicator;
  guint16 program_number;
  GstFluTSStream * pcr_stream;

  demux = stream->demux;
  klass = GST_FLUTS_DEMUX_GET_CLASS (demux);

  if (G_UNLIKELY (*data++ != 0x02))
    goto wrong_id;
  if ((data[0] & 0xc0) != 0x80)
    goto wrong_sync;
  if ((data[0] & 0x0c) != 0x00)
    goto wrong_seclen;

  data += 2;

  if (demux->check_crc)
    if (G_UNLIKELY (gst_fluts_demux_calc_crc32 (data - 3, datalen) != 0))
      goto wrong_crc;

  GST_LOG_OBJECT (demux, "PMT section_length: %d", datalen - 3);

  PMT = &stream->PMT;

  /* check if version number changed */
  version_number = (data[2] & 0x3e) >> 1;
  GST_LOG_OBJECT (demux, "PMT version_number: %d", version_number);

  current_next_indicator = (data[2] & 0x01);
  GST_LOG_OBJECT (demux, "PMT current_next_indicator %d",
      current_next_indicator);
  if (current_next_indicator == 0)
    goto not_yet_applicable;
  program_number = GST_READ_UINT16_BE (data);

  if (demux->program_number != -1 && demux->program_number != program_number) {
    goto wrong_program_number;
  }
  if (demux->program_number == -1) {
    GST_INFO_OBJECT (demux, "No program number set, so using first parsed PMT"
        "'s program number: %d", program_number);
    demux->program_number = program_number;
  }

  if (version_number == PMT->version_number)
    goto same_version;

  PMT->version_number = version_number;
  PMT->current_next_indicator = current_next_indicator;

  stream->PMT.program_number = program_number;
  data += 3;
  GST_DEBUG_OBJECT (demux, "PMT program_number: %d", PMT->program_number);

  PMT->section_number = *data++;
  GST_DEBUG_OBJECT (demux, "PMT section_number: %d", PMT->section_number);

  PMT->last_section_number = *data++;
  GST_DEBUG_OBJECT (demux, "PMT last_section_number: %d",
      PMT->last_section_number);

  PMT->PCR_PID = GST_READ_UINT16_BE (data);
  PMT->PCR_PID &= 0x1fff;
  data += 2;
  GST_DEBUG_OBJECT (demux, "PMT PCR_PID: 0x%04x", PMT->PCR_PID);
  /* create or get stream, not much we can say about it except that when we get
   * a data stream and we need a PCR, we can use the stream to get/store the
   * base_PCR. */
  pcr_stream = gst_fluts_demux_get_stream_for_PID (demux, PMT->PCR_PID);
  if (pcr_stream->PID_type == PID_TYPE_UNKNOWN) {
    /* Make sure we initialize the PCR stream properly in case it is not
     * present in the entries of this PMT. Indeed we need to have a consistent
     * base time for all the PES streams for the newsegment event */
    pcr_stream->base_time = demux->pts_offset;
  }

  if ((data[0] & 0x0c) != 0x00)
    goto wrong_pilen;

  PMT->program_info_length = GST_READ_UINT16_BE (data);
  PMT->program_info_length &= 0x0fff;
  /* FIXME: validate value of program_info_length */
  data += 2;

  /* FIXME: validate value of program_info_length, before using */

  /* parse descriptor */
  if (G_UNLIKELY (PMT->program_info))
    gst_mpeg_descriptor_free (PMT->program_info);
  PMT->program_info =
      gst_mpeg_descriptor_parse (data, PMT->program_info_length);

  /* skip descriptor */
  data += PMT->program_info_length;
  GST_DEBUG_OBJECT (demux, "PMT program_info_length: %d",
      PMT->program_info_length);

  entries = datalen - 3 - PMT->program_info_length - 9 - 4;

  if (G_UNLIKELY (PMT->entries))
    g_array_free (PMT->entries, TRUE);
  PMT->entries = g_array_new (FALSE, TRUE, sizeof (GstFluTSPMTEntry));

  while (entries > 0) {
    GstFluTSPMTEntry entry;
    GstFluTSStream *ES_stream;
    guint8 stream_type;
    guint16 ES_info_length;

    stream_type = *data++;

    entry.PID = GST_READ_UINT16_BE (data);
    entry.PID &= 0x1fff;
    data += 2;

    if ((data[0] & 0x0c) != 0x00)
      goto wrong_esilen;

    ES_info_length = GST_READ_UINT16_BE (data);
    ES_info_length &= 0x0fff;
    data += 2;

    /* get/create elementary stream */
    ES_stream = gst_fluts_demux_get_stream_for_PID (demux, entry.PID);
    /* check if PID unknown */
    if (ES_stream->PID_type == PID_TYPE_UNKNOWN) {
      /* set as elementary */
      ES_stream->PID_type = PID_TYPE_ELEMENTARY;
      /* set stream type */
      ES_stream->stream_type = stream_type;
      ES_stream->flags &= ~FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;

      /* init base and last time */
      ES_stream->base_time = demux->pts_offset;
      ES_stream->last_time = 0;

      /* parse descriptor */
      ES_stream->ES_info = gst_mpeg_descriptor_parse (data, ES_info_length);

      if (stream_type == ST_PRIVATE_SECTIONS) {
        /* not really an ES, so use section filter not pes filter */
        /* initialise section filter */
        GstCaps *caps;
        gchar *pad_name = g_strdup_printf ("private_%04x", entry.PID);
        gst_section_filter_init (&ES_stream->section_filter);
        ES_stream->PID_type = PID_TYPE_PRIVATE_SECTION;
        ES_stream->pad = gst_pad_new_from_static_template (&private_template,
            pad_name);
        g_free (pad_name);
        gst_pad_set_active (ES_stream->pad, TRUE);
        caps = gst_caps_new_simple ("application/x-mpegts-private-section",
            NULL);
        gst_pad_use_fixed_caps (ES_stream->pad);
        gst_pad_set_caps (ES_stream->pad, caps);
        gst_caps_unref (caps);

        gst_element_add_pad (GST_ELEMENT_CAST (demux), ES_stream->pad);
      } else if (stream_type == ST_MPEG4_SECTION) {
        gst_section_filter_init (&ES_stream->section_filter);
        ES_stream->PID_type = PID_TYPE_MPEG4_SECTION;
        ES_stream->PMT_pid = stream->PID;
      } else {
        /* Recognise video streams based on stream_type */
        if (gst_fluts_stream_is_video (ES_stream))
          ES_stream->flags |= FLUTS_STREAM_FLAG_IS_VIDEO;

        /* set adaptor */
        gst_pes_filter_init (&ES_stream->filter, NULL, NULL);
        gst_pes_filter_set_callbacks (&ES_stream->filter,
            (GstPESFilterData) gst_fluts_demux_data_cb,
            (GstPESFilterResync) gst_fluts_demux_resync_cb, ES_stream);
        if (ES_stream->flags & FLUTS_STREAM_FLAG_IS_VIDEO)
          ES_stream->filter.allow_unbounded = TRUE;
        ES_stream->PMT_pid = stream->PID;

        /* Figure out information about stream type */
        gst_fluts_demux_fill_stream (ES_stream, ES_stream->filter.id,
            ES_stream->stream_type);

        /* By default we activate on data */
        ES_stream->activation = FLUTS_STREAM_ACTIVATE_ON_DATA;
      }
    }

    if (ES_stream->template == klass->video_template) {
      nb_videos++;
      if (demux->playbin2 && nb_videos > 1) {
        ES_stream->activation = FLUTS_STREAM_NOT_ACTIVATED;
      }
    }
    else if (ES_stream->template == klass->audio_template) {
      nb_audios++;
      if (demux->playbin2 && nb_audios > 1) {
        ES_stream->activation = FLUTS_STREAM_NOT_ACTIVATED;
      }

      if (stream_type == ST_AUDIO_MPEG1)
        ES_stream->codec = "MPEG-1 layer 3";
      
      if (stream_type == ST_AUDIO_AAC_14496_3 && ES_stream->ES_info) {
        ES_stream->codec = "MPEG-4 AAC audio";
      }
    }

    if (stream_type == ST_PRIVATE_DATA && ES_stream->ES_info) {
      if (gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_DVB_AC3)) {
        ES_stream->codec = "AC-3 audio";
      } else if (gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_DVB_DTS)) {
        ES_stream->codec = "DTS";
      }
    }

    /* Subtitles are not supported in this demuxer yet, but let's leave
       the code to set AVComponent encoding tag here: */
    if (!ES_stream->codec && ES_stream->ES_info) {
      const guint8 desc_entry_size = 8,
      *desc = gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_DVB_SUBTITLING);
      if (desc) {
        guint8 desc_len = DESC_LENGTH (desc);
        desc += 2;
        while (desc_len > 0 && !ES_stream->codec) {
          /*
             ISO_639_language_code    24                    bslbf
             subtitling_type          8                     bslbf
             composition_page_id      16                    bslbf
             ancillary_page_id        16                    bslbf
          */
          const guint8 subtitling_type = desc[3];
          switch (subtitling_type) {
            case 0x01:
              ES_stream->codec = "EBU-SUBT";
              break;
            case 0x20:
            case 0x21:
            case 0x22:
            case 0x24:
              ES_stream->for_hearing_impaired = TRUE;
            case 0x10:
            case 0x11:
            case 0x12:
            case 0x14:
              ES_stream->codec = "DVB-SUBT";
              break;
            default:
              break;
          }
          
          desc_len -= desc_entry_size;
          desc += desc_entry_size;
        }
      }

      if (!ES_stream->codec) {
        const guint8 *desc = gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_CAPTION_SERVICE);
        if (desc) {
          /*
             ATSC A/65A [4], Table 6.26
             Bit Stream Syntax for the Caption Service Descriptor

             Syntax                   No. of Bits          Format
             caption_service_descriptor ()
             {
             descriptor_tag                8               0x86
             descriptor_length             8               uimsbf
             reserved                      3               ‘111’
             number_of_services            5               uimsbf
             for (i=0; i<number_of_services; i++) {
               language                    8*3             uimsbf
               digital_cc                  1               bslbf
               reserved                    1               ‘1’
               if (digital_cc == 0) {
                 reserved                  5               ‘11111’
                 line21_field              1               bslbf
               }
               else
                 caption_service_number    6               uimsbf
               easy_reader                 1               bslbf
               wide_aspect_ratio           1               bslbf
               reserved                    14              ‘11111111111111’
          */
          guint8 num_of_services = desc[2] & 0x1F;
          gint8 len = DESC_LENGTH (desc) - 1;
          desc += 3;
          for (;num_of_services > 0 && len > 0 && !ES_stream->codec;
               num_of_services --, desc += 6, len -= 6) {
            gboolean digital_cc = !!(desc[3] & 0x80);
            if (digital_cc == 0) {
              ES_stream->codec = "CEA-SUBT";
              break;
            }
          }
        }
      } // if (!ES_stream->codec)
    } // if (!ES_stream->codec)

    /* Fill AVComponent's component_tag: */
    if (ES_stream->ES_info) {
      const guint8 *desc =
        gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_DVB_STREAM_IDENTIFIER);
      if (desc)
        ES_stream->component_tag = desc[2];
    }
    
    if (ES_stream->ES_info) {
      guint8 *desc =
          gst_mpeg_descriptor_find (ES_stream->ES_info, DESC_CA);
      if (desc) {
        GstFluTSStream *CA_stream;
        ES_stream->is_encrypted = TRUE;
        ES_stream->CA_pid = DESC_CA_PID (desc);
        CA_stream =
            gst_fluts_demux_get_stream_for_PID (demux, ES_stream->CA_pid);
        CA_stream->PMT_pid = stream->PID;
        if (CA_stream->PID_type == PID_TYPE_UNKNOWN) {
          GstFluTSPMTEntry ca_entry;
          CA_stream->PID_type = PID_TYPE_ELEMENTARY;

          gst_pes_filter_init (&CA_stream->filter, NULL, NULL);
          gst_pes_filter_set_callbacks (&CA_stream->filter,
              (GstPESFilterData) gst_fluts_demux_data_cb,
              (GstPESFilterResync) gst_fluts_demux_resync_cb, CA_stream);

          ca_entry.PID = ES_stream->CA_pid;

          g_array_append_val (PMT->entries, ca_entry);
        }
      }
    }

    /* skip descriptor */
    data += ES_info_length;
    GST_DEBUG_OBJECT (demux,
        "  PMT stream_type: 0x%02x, PID: 0x%04x (ES_info_len %d) caps: %" GST_PTR_FORMAT,
        stream_type, entry.PID, ES_info_length, ES_stream->caps);
    if (ES_stream->ES_info) {
      GST_MEMDUMP_OBJECT (demux, "ES_info",
          ES_stream->ES_info->data, ES_stream->ES_info->data_length);
    }

    g_array_append_val (PMT->entries, entry);

    entries -= 5 + ES_info_length;
  }
  CRC = GST_READ_UINT32_BE (data);
  GST_DEBUG_OBJECT (demux, "PMT CRC: 0x%08x", CRC);

  if (demux->program_number == -1) {
    /* No program specified, take the first PMT */
    if (demux->current_PMT == 0 || demux->current_PMT == stream->PID)
      gst_fluts_activate_pmt (demux, stream);
  } else {
    /* Program specified, activate this if it matches */
    if (demux->program_number == PMT->program_number)
      gst_fluts_activate_pmt (demux, stream);
  }

  gst_element_post_message (GST_ELEMENT (demux),
      gst_message_new_element (GST_OBJECT (demux),
          gst_fluts_build_pmt_structure (demux, PMT)));

  return TRUE;

  /* ERRORS */
wrong_crc:
  {
    GST_DEBUG_OBJECT (demux, "wrong crc");
    return FALSE;
  }
same_version:
  {
    GST_DEBUG_OBJECT (demux, "same version as existing PMT");
    return TRUE;
  }
wrong_program_number:
  {
    GST_DEBUG_OBJECT (demux, "PMT is for program number we don't care about");
    return TRUE;
  }

not_yet_applicable:
  {
    GST_DEBUG_OBJECT (demux, "Ignoring PMT with current_next_indicator = 0");
    return TRUE;
  }
wrong_id:
  {
    GST_DEBUG_OBJECT (demux, "expected table_id == 0, got 0x%02x", data[0]);
    return FALSE;
  }
wrong_sync:
  {
    GST_DEBUG_OBJECT (demux, "expected sync 10, got %02x", data[0]);
    return FALSE;
  }
wrong_seclen:
  {
    GST_DEBUG_OBJECT (demux,
        "first two bits of section length must be 0, got %02x", data[0]);
    return FALSE;
  }
wrong_pilen:
  {
    GST_DEBUG_OBJECT (demux,
        "first two bits of program_info length must be 0, got %02x", data[0]);
    return FALSE;
  }
wrong_esilen:
  {
    GST_DEBUG_OBJECT (demux,
        "first two bits of ES_info length must be 0, got %02x", data[0]);
    g_array_free (stream->PMT.entries, TRUE);
    stream->PMT.entries = NULL;
    gst_mpeg_descriptor_free (stream->PMT.program_info);
    stream->PMT.program_info = NULL;
    return FALSE;
  }
}

/*
 * private_section() {
 *   table_id                                       8 uimsbf
 *   section_syntax_indicator                       1 bslbf
 *   private_indicator                              1 bslbf
 *   reserved                                       2 bslbf
 *   private_section_length                        12 uimsbf
 *   if (section_syntax_indicator == '0') {
 *     for ( i=0;i<N;i++) {
 *       private_data_byte                          8 bslbf
 *     }
 *   }
 *   else {
 *     table_id_extension                          16 uimsbf
 *     reserved                                     2 bslbf
 *     version_number                               5 uimsbf
 *     current_next_indicator                       1 bslbf
 *     section_number                               8 uimsbf
 *     last_section_number                          8 uimsbf
 *     for ( i=0;i<private_section_length-9;i++) {
 *       private_data_byte                          8 bslbf
 *     }
 *     CRC_32                                      32 rpchof
 *   }
 * }
 */
static FORCE_INLINE gboolean
gst_fluts_stream_parse_private_section (GstFluTSStream * stream,
    guint8 * data, guint datalen)
{
  GstFluTSDemux *demux;
  GstBuffer *buffer;
  demux = stream->demux;

  if (demux->check_crc)
    if (gst_fluts_demux_calc_crc32 (data, datalen) != 0)
      goto wrong_crc;

  /* just dump this down the pad */
  if (gst_pad_alloc_buffer (stream->pad, 0, datalen, NULL, &buffer) ==
      GST_FLOW_OK) {
#ifdef USE_LIBOIL
    oil_memcpy (buffer->data, data, datalen);
#else
    memcpy (buffer->data, data, datalen);
#endif
    gst_pad_push (stream->pad, buffer);
  }

  GST_DEBUG_OBJECT (demux, "parsing private section");
  return TRUE;

wrong_crc:
  {
    GST_DEBUG_OBJECT (demux, "wrong crc");
    return FALSE;
  }
}

/*
 * adaptation_field() {
 *   adaptation_field_length                              8 uimsbf
 *   if(adaptation_field_length >0) {
 *     discontinuity_indicator                            1 bslbf
 *     random_access_indicator                            1 bslbf
 *     elementary_stream_priority_indicator               1 bslbf
 *     PCR_flag                                           1 bslbf
 *     OPCR_flag                                          1 bslbf
 *     splicing_point_flag                                1 bslbf
 *     transport_private_data_flag                        1 bslbf
 *     adaptation_field_extension_flag                    1 bslbf
 *     if(PCR_flag == '1') {
 *       program_clock_reference_base                    33 uimsbf
 *       reserved                                         6 bslbf
 *       program_clock_reference_extension                9 uimsbf
 *     }
 *     if(OPCR_flag == '1') {
 *       original_program_clock_reference_base           33 uimsbf
 *       reserved                                         6 bslbf
 *       original_program_clock_reference_extension       9 uimsbf
 *     }
 *     if (splicing_point_flag == '1') {
 *       splice_countdown                                 8 tcimsbf
 *     }
 *     if(transport_private_data_flag == '1') {
 *       transport_private_data_length                    8 uimsbf
 *       for (i=0; i<transport_private_data_length;i++){
 *         private_data_byte                              8 bslbf
 *       }
 *     }
 *     if (adaptation_field_extension_flag == '1' ) {
 *       adaptation_field_extension_length                8 uimsbf
 *       ltw_flag                                         1 bslbf
 *       piecewise_rate_flag                              1 bslbf
 *       seamless_splice_flag                             1 bslbf
 *       reserved                                         5 bslbf
 *       if (ltw_flag == '1') {
 *         ltw_valid_flag                                 1 bslbf
 *         ltw_offset                                    15 uimsbf
 *       }
 *       if (piecewise_rate_flag == '1') {
 *         reserved                                       2 bslbf
 *         piecewise_rate                                22 uimsbf
 *       }
 *       if (seamless_splice_flag == '1'){
 *         splice_type                                    4 bslbf
 *         DTS_next_AU[32..30]                            3 bslbf
 *         marker_bit                                     1 bslbf
 *         DTS_next_AU[29..15]                           15 bslbf
 *         marker_bit                                     1 bslbf
 *         DTS_next_AU[14..0]                            15 bslbf
 *         marker_bit                                     1 bslbf
 *       }
 *       for ( i=0;i<N;i++) {
 *         reserved                                       8 bslbf
 *       }
 *     }
 *     for (i=0;i<N;i++){
 *       stuffing_byte                                    8 bslbf
 *     }
 *   }
 * }
 */
static FORCE_INLINE gboolean
gst_fluts_demux_parse_adaptation_field (GstFluTSStream * stream,
    const guint8 * data, guint data_len, guint * consumed)
{
  GstFluTSDemux *demux;
  guint8 length;
  guint8 *data_end;
  GstFluTSStream *pmt_stream;

  demux = stream->demux;

  data_end = ((guint8 *) data) + data_len;

  length = *data++;
  if (G_UNLIKELY (length > data_len))
    goto wrong_length;

  GST_DEBUG_OBJECT (demux, "parsing adaptation field, length %d", length);

  if (length > 0) {
    guint8 flags = *data++;

    GST_LOG_OBJECT (demux, "flags 0x%02x", flags);
    /* discontinuity flag */
    if (flags & 0x80) {
      GST_DEBUG_OBJECT (demux, "discontinuity flag set");
    }

    if (flags & 0x40) {
      GST_DEBUG_OBJECT (demux, "random access indicator flag set");
    }
    /* PCR_flag */
    if (flags & 0x10) {
      guint32 pcr1;
      guint16 pcr2;
      guint64 pcr, pcr_ext;
      gboolean valid_pcr = TRUE;
      gboolean pcr_discont = FALSE;

      pcr1 = GST_READ_UINT32_BE (data);
      pcr2 = GST_READ_UINT16_BE (data + 4);
      pcr = ((guint64) pcr1) << 1;
      pcr |= (pcr2 & 0x8000) >> 15;
      pcr_ext = (pcr2 & 0x01ff);
      if (pcr_ext)
        pcr = (pcr * 300 + pcr_ext % 300) / 300;
      GST_DEBUG_OBJECT (demux,
          "have PCR %" G_GUINT64_FORMAT "(%" GST_TIME_FORMAT ") on PID 0x%04x "
          "and last pcr is %" G_GUINT64_FORMAT " (%" GST_TIME_FORMAT ")", pcr,
          GST_TIME_ARGS (MPEGTIME_TO_GSTTIME (pcr)), stream->PID,
          stream->last_PCR,
          GST_TIME_ARGS (MPEGTIME_TO_GSTTIME (stream->last_PCR)));
      /* We will now check the PCR observations for discontinuity, this can
       * only be done if we have some previous observation. */
      if (G_LIKELY(stream->last_PCR != -1)) {
        /* We can tolerate a gap up to 10 seconds. When doing trick modes
         * we have to take the rate in account as the stream could contain
         * only keyframes for example. */
        guint64 discont_window = 900000 * ABS(demux->requested_rate);
        /* take into account the playback direction when checking for
         * a PCR discontinuity and properly detect reverse playback. */
        if (demux->requested_rate > 0) {
          /* PCR has been converted into units of 90Khz ticks so assume discont
           * in forward if last pcr was > 900000 (10 second) lower or
           * of it wrapped compared to the previous one */
          pcr_discont =
              (pcr < stream->last_PCR || (pcr - stream->last_PCR) > discont_window);
        } else {
          /* PCR has been converted into units of 90Khz ticks so assume discont
           * in reverse if last pcr was > 900000 (10 second) lower or
           * of it wrapped for more than 2 secconds */
          if (pcr > stream->last_PCR) {
            pcr_discont = (pcr - stream->last_PCR) > 900000;
          } else {
            /* consider a PCR wraparound when it gone lower for more than 10
             * seconds. */
            pcr_discont = (stream->last_PCR - pcr) > discont_window;
          }
        }

        if (pcr_discont) {

          GST_DEBUG_OBJECT (demux,
              "looks like we have a discont, this pcr should really be approx: %"
              G_GUINT64_FORMAT, stream->last_PCR + stream->avg_PCR_difference);
          /* When this PCR value triggers a discontinuity we will check the
           * next one too to be sure that a bitstream corruption is not
           * misinterpreted */
          if (stream->discont_PCR == FALSE) {
            /* First PCR that triggers a discont is just ignored and we
             * set stream->discont_PCR to TRUE on order to initiate a clock
             * recovery */
            stream->discont_PCR = TRUE;
            valid_pcr = FALSE;
            if (pcr > stream->last_PCR) {
              stream->discont_difference = -MPEGTIME_TO_GSTTIME ((pcr -
                      (stream->last_PCR + stream->avg_PCR_difference)));
            } else {
              stream->discont_difference = MPEGTIME_TO_GSTTIME ((stream->last_PCR +
                      stream->avg_PCR_difference) - pcr);
            }
          } else {
            GstClockTimeDiff base_time_difference;

            /* If a second PCR triggers a discont then a PCR correction is
             * calculated */
            gint j;
            gboolean need_drain = FALSE;
            gboolean *pmts_checked = (gboolean *) & demux->pmts_checked;
            memset (pmts_checked, 0, sizeof (gboolean) * (FLUTS_MAX_PID + 1));
            /* need to update all pmt streams in case this pcr is pcr
             * for multiple programs */
            for (j = 0; j < FLUTS_MAX_PID + 1; j++) {
              if (demux->streams[j]
                  && demux->streams[j]->PMT_pid <= FLUTS_MAX_PID) {
                if (!pmts_checked[demux->streams[j]->PMT_pid]) {
                  /* check if this is correct pcr for pmt */
                  if (demux->streams[demux->streams[j]->PMT_pid] &&
                      stream->PID ==
                      demux->streams[demux->streams[j]->PMT_pid]->PMT.PCR_PID) {
                    /* checking the pcr discont is similar this second time
                     * if similar, update the es pids
                     * if not, assume it's a false discont due to corruption
                     * or other */
                    if (pcr > stream->last_PCR) {
                      base_time_difference = -MPEGTIME_TO_GSTTIME ((pcr -
                              (stream->last_PCR + stream->avg_PCR_difference)));
                    } else {
                      base_time_difference =
                          MPEGTIME_TO_GSTTIME ((stream->last_PCR +
                              stream->avg_PCR_difference) - pcr);
                    }
                    if ((base_time_difference - stream->discont_difference > 0 &&
                            base_time_difference - stream->discont_difference <
                            GST_SECOND * 10) ||
                        (stream->discont_difference - base_time_difference > 0 &&
                            stream->discont_difference - base_time_difference <
                            GST_SECOND * 10)) {
                      pmt_stream = demux->streams[demux->streams[j]->PMT_pid];
#ifdef HAVE_LATENCY
                      if (demux->is_live) {
                        GST_DEBUG_OBJECT (demux, "Discontinuity detected we will drain");
                        need_drain = TRUE;
                      } else
#endif
                      {
                        GST_DEBUG_OBJECT (demux,
                            "Updating base time on PMT pid 0x%02x by %"
                            G_GINT64_FORMAT, demux->streams[j]->PMT_pid,
                            stream->discont_difference);
                        /* Update PMT base time to reflect the PCR discont */
                        pmt_stream->base_time += stream->discont_difference;
                      }
                    } else {
                      GST_DEBUG_OBJECT (demux, "last PCR discont looked to be "
                          "bogus: previous discont difference %" G_GINT64_FORMAT
                          " now %" G_GINT64_FORMAT, stream->discont_difference,
                          base_time_difference);
                      valid_pcr = FALSE;
                    }
                  }
                }
                pmts_checked[demux->streams[j]->PMT_pid] = TRUE;
              }              
            }
            if (need_drain) {
              gst_fluts_demux_flush (demux, FALSE);
            }
            stream->discont_PCR = FALSE;
            stream->discont_difference = 0;
          }
        } else {
          if (stream->discont_PCR) {
            GST_DEBUG_OBJECT (demux, "last PCR discont looked to be bogus");
            stream->discont_PCR = FALSE;
            stream->discont_difference = 0;
          }
          if (pcr > stream->last_PCR) {
            if (stream->avg_PCR_difference) {
              stream->avg_PCR_difference += pcr - stream->last_PCR;
              stream->avg_PCR_difference >>= 1;
            } else {
              stream->avg_PCR_difference = pcr - stream->last_PCR;
            }
          }
        }
      }
      GST_DEBUG_OBJECT (demux,
          "valid pcr: %d avg PCR difference: %" G_GUINT64_FORMAT, valid_pcr,
          stream->avg_PCR_difference);
      /* keep the last PCR to determine
       * the discontinuity with the next pcr.
       * Need to check that the discont is due to a real PCR regression
       * or exceeding the discont_window again.
       */
      stream->last_PCR = pcr;
      if (valid_pcr) {
        GstFluTSStream *PMT_stream = demux->streams[demux->current_PMT];

        if (G_UNLIKELY (PMT_stream && PMT_stream->PMT.PCR_PID == 0x1fff)) {
          GST_DEBUG_OBJECT (demux, "reconfigured the PCR pid with first valid");
          PMT_stream->PMT.PCR_PID = stream->PID;
        }

        /* Start recording bitrate info when we've started pushing data. */
        if (PMT_stream && PMT_stream->PMT.PCR_PID == stream->PID &&
            demux->pushing) {
          if (demux->pcr[0] == -1) {
            GST_DEBUG_OBJECT (demux, "RECORDING pcr[0]:%" G_GUINT64_FORMAT,
                pcr);
            demux->pcr[0] = pcr;
            demux->num_packets = 0;
          } /* Considering a difference in 90 ticks unit (90Khz) */
          else if (G_UNLIKELY (demux->pcr[1] == -1 &&
              ((pcr - demux->pcr[0]) >= 90 * demux->pcr_difference))) {
            GST_DEBUG_OBJECT (demux, "RECORDING pcr[1]:%" G_GUINT64_FORMAT,
                pcr);
            demux->pcr[1] = pcr;
          }
        }

#ifdef HAVE_LATENCY
        if (demux->clock && demux->clock_base != GST_CLOCK_TIME_NONE) {
          gdouble r_squared;
          GstFluTSStream *PMT_stream;

          /* for the reference start time we need to consult the PCR_PID of the
           * current PMT */
          PMT_stream = demux->streams[demux->current_PMT];
          if (PMT_stream->PMT.PCR_PID == stream->PID) {
            GST_LOG_OBJECT (demux,
                "internal %" GST_TIME_FORMAT " observation %" GST_TIME_FORMAT
                " pcr: %" G_GUINT64_FORMAT " base_pcr: %" G_GUINT64_FORMAT
                "pid: %d",
                GST_TIME_ARGS (gst_clock_get_internal_time (demux->clock)),
                GST_TIME_ARGS (MPEGTIME_TO_GSTTIME (pcr) -
                    MPEGTIME_TO_GSTTIME (stream->base_PCR) +
                    PMT_stream->base_time + stream->base_time +
                    demux->clock_base), pcr, stream->base_PCR, stream->PID);
            gst_clock_add_observation (demux->clock,
                gst_clock_get_internal_time (demux->clock),
                demux->clock_base + PMT_stream->base_time + stream->base_time +
                MPEGTIME_TO_GSTTIME (pcr) -
                MPEGTIME_TO_GSTTIME (stream->base_PCR), &r_squared);
          }
        }
#endif
      }
      data += 6;
    }
    /* OPCR_flag */
    if (flags & 0x08) {
      guint32 opcr1;
      guint16 opcr2;
      guint64 opcr, opcr_ext;

      opcr1 = GST_READ_UINT32_BE (data);
      opcr2 = GST_READ_UINT16_BE (data + 4);
      opcr = ((guint64) opcr1) << 1;
      opcr |= (opcr2 & 0x8000) >> 15;
      opcr_ext = (opcr2 & 0x01ff);
      if (opcr_ext)
        opcr = (opcr * 300 + opcr_ext % 300) / 300;
      GST_DEBUG_OBJECT (demux, "have OPCR %" G_GUINT64_FORMAT " on PID 0x%04x",
          opcr, stream->PID);
      stream->last_OPCR = opcr;
      data += 6;
    }
    /* splicing_point_flag */
    if (flags & 0x04) {
      guint8 splice_countdown;

      splice_countdown = *data++;
      GST_DEBUG_OBJECT (demux, "have splicing point, countdown %d",
          splice_countdown);
    }
    /* transport_private_data_flag */
    if (flags & 0x02) {
      guint8 plength = *data++;

      if (data + plength > data_end)
        goto private_data_too_large;

      GST_DEBUG_OBJECT (demux, "have private data, length: %d", plength);
      data += plength;
    }
    /* adaptation_field_extension_flag */
    if (flags & 0x01) {
      GST_DEBUG_OBJECT (demux, "have field extension");
    }
  }

  *consumed = length + 1;
  return TRUE;

  /* ERRORS */
wrong_length:
  {
    GST_DEBUG_OBJECT (demux, "length %d > %d", length, data_len);
    return FALSE;
  }
private_data_too_large:
  {
    GST_DEBUG_OBJECT (demux, "have too large a private data length");
    return FALSE;
  }
}

/*
 * program_association_section() {
 *   table_id                               8 uimsbf   == 0x00
 *   section_syntax_indicator               1 bslbf    == 1
 *   '0'                                    1 bslbf    == 0
 *   reserved                               2 bslbf
 *   section_length                        12 uimsbf   == 00xxxxx...
 *   transport_stream_id                   16 uimsbf
 *   reserved                               2 bslbf
 *   version_number                         5 uimsbf
 *   current_next_indicator                 1 bslbf
 *   section_number                         8 uimsbf
 *   last_section_number                    8 uimsbf
 *   for (i=0; i<N;i++) {
 *     program_number                      16 uimsbf
 *     reserved                             3 bslbf
 *     if(program_number == '0') {
 *       network_PID                       13 uimsbf
 *     }
 *     else {
 *       program_map_PID                   13 uimsbf
 *     }
 *   }
 *   CRC_32                                32 rpchof
 * }
 */
static FORCE_INLINE gboolean
gst_fluts_stream_parse_pat (GstFluTSStream * stream,
    guint8 * data, guint datalen)
{
  GstFluTSDemux *demux;
  gint entries;
  guint32 CRC;
  guint version_number;
  guint8 current_next_indicator;
  GstFluTSPAT *PAT;

  demux = stream->demux;

  if (datalen < 8)
    return FALSE;

  if (*data++ != 0x00)
    goto wrong_id;
  if ((data[0] & 0xc0) != 0x80)
    goto wrong_sync;
  if (G_UNLIKELY ((data[0] & 0x0c) != 0x00))
    goto wrong_seclen;

  data += 2;
  GST_DEBUG_OBJECT (demux, "PAT section_length: %d", datalen - 3);

  if (demux->check_crc)
    if (gst_fluts_demux_calc_crc32 (data - 3, datalen) != 0)
      goto wrong_crc;

  PAT = &stream->PAT;

  version_number = (data[2] & 0x3e) >> 1;
  GST_DEBUG_OBJECT (demux, "PAT version_number: %d", version_number);
  if (G_UNLIKELY (version_number == PAT->version_number))
    goto same_version;

  current_next_indicator = (data[2] & 0x01);
  GST_DEBUG_OBJECT (demux, "PAT current_next_indicator %d",
      current_next_indicator);
  if (current_next_indicator == 0)
    goto not_yet_applicable;

  PAT->version_number = version_number;
  PAT->current_next_indicator = current_next_indicator;

  PAT->transport_stream_id = GST_READ_UINT16_BE (data);
  data += 3;
  GST_DEBUG_OBJECT (demux, "PAT stream_id: %d", PAT->transport_stream_id);

  PAT->section_number = *data++;
  PAT->last_section_number = *data++;

  GST_DEBUG_OBJECT (demux, "PAT current_next_indicator: %d",
      PAT->current_next_indicator);
  GST_DEBUG_OBJECT (demux, "PAT section_number: %d", PAT->section_number);
  GST_DEBUG_OBJECT (demux, "PAT last_section_number: %d",
      PAT->last_section_number);

  /* 5 bytes after section length and a 4 bytes CRC,
   * the rest is 4 byte entries */
  entries = (datalen - 3 - 9) / 4;

  if (PAT->entries)
    g_array_free (PAT->entries, TRUE);
  PAT->entries =
      g_array_sized_new (FALSE, TRUE, sizeof (GstFluTSPATEntry), entries);

  while (entries--) {
    GstFluTSPATEntry entry;
    GstFluTSStream *PMT_stream;

    entry.program_number = GST_READ_UINT16_BE (data);
    data += 2;
    entry.PID = GST_READ_UINT16_BE (data);
    entry.PID &= 0x1fff;
    data += 2;

    /* get/create stream for PMT */
    PMT_stream = gst_fluts_demux_get_stream_for_PID (demux, entry.PID);
    if (PMT_stream->PID_type != PID_TYPE_PROGRAM_MAP) {
      /* set as program map */
      PMT_stream->PID_type = PID_TYPE_PROGRAM_MAP;
      /* initialise section filter */
      gst_section_filter_init (&PMT_stream->section_filter);
    }

    g_array_append_val (PAT->entries, entry);

    GST_DEBUG_OBJECT (demux, "  PAT program: %d, PID 0x%04x",
        entry.program_number, entry.PID);
  }
  CRC = GST_READ_UINT32_BE (data);
  GST_DEBUG_OBJECT (demux, "PAT CRC: 0x%08x", CRC);

  /* PAT has been updated, send a bus message and signal the change */
  gst_element_post_message (GST_ELEMENT (demux),
      gst_message_new_element (GST_OBJECT (demux),
          gst_fluts_build_pat_structure (demux, PAT)));
  g_object_notify ((GObject *) (demux), "pat-info");

  return TRUE;

  /* ERRORS */
wrong_crc:
  {
    GST_DEBUG_OBJECT (demux, "wrong crc");
    return FALSE;
  }
same_version:
  {
    GST_DEBUG_OBJECT (demux, "same version as existing PAT");
    return TRUE;
  }
not_yet_applicable:
  {
    GST_DEBUG_OBJECT (demux, "ignoring PAT with current_next_indicator = 0");
    return TRUE;
  }
wrong_id:
  {
    GST_DEBUG_OBJECT (demux, "expected table_id == 0, got %02x", data[0]);
    return FALSE;
  }
wrong_sync:
  {
    GST_DEBUG_OBJECT (demux, "expected sync 10, got %02x", data[0]);
    return FALSE;
  }
wrong_seclen:
  {
    GST_DEBUG_OBJECT (demux,
        "first two bits of section length must be 0, got %02x", data[0]);
    return FALSE;
  }
}

static gboolean
gst_fluts_demux_is_PMT (GstFluTSDemux * demux, guint16 PID)
{
  GstFluTSStream *stream;
  GstFluTSPAT *PAT;
  gint i;

  /* get the PAT */
  stream = demux->streams[PID_PROGRAM_ASSOCIATION_TABLE];
  if (stream == NULL || stream->PAT.entries == NULL)
    return FALSE;

  PAT = &stream->PAT;

  for (i = 0; i < PAT->entries->len; i++) {
    GstFluTSPATEntry *entry;

    entry = &g_array_index (PAT->entries, GstFluTSPATEntry, i);

    if (entry->PID == PID)
      return TRUE;
  }
  return FALSE;
}

static FORCE_INLINE GstFlowReturn
gst_fluts_stream_pes_buffer_flush (GstFluTSStream * stream, gboolean discard)
{
  GstFlowReturn ret = GST_FLOW_OK;

  if (stream->pes_buffer) {
    if (discard) {
      gst_buffer_unref (stream->pes_buffer);
      stream->pes_buffer_in_sync = FALSE;
    } else {
      GST_BUFFER_SIZE (stream->pes_buffer) = stream->pes_buffer_used;
      ret = gst_pes_filter_push (&stream->filter, stream->pes_buffer);
      if (ret == GST_FLOW_LOST_SYNC)
        stream->pes_buffer_in_sync = FALSE;
    }
    stream->pes_buffer = NULL;
  }
  return ret;
}

static FORCE_INLINE GstFlowReturn
gst_fluts_stream_pes_buffer_push (GstFluTSDemux * demux,
    GstFluTSStream * stream, const guint8 * in_data, guint in_size)
{
  GstFlowReturn ret = GST_FLOW_OK;
  guint8 *out_data;

  if (G_UNLIKELY (stream->pes_buffer
          && stream->pes_buffer_used + in_size > stream->pes_buffer_size)) {
    GST_DEBUG ("stream with PID 0x%04x have PES buffer full at %u bytes."
        " Flushing and growing the buffer",
        stream->PID, stream->pes_buffer_size);
    stream->pes_buffer_overflow = TRUE;
    if (stream->pes_buffer_size < (demux->max_pes_buffer_size >> 1))
      stream->pes_buffer_size <<= 1;

    ret = gst_fluts_stream_pes_buffer_flush (stream, FALSE);
    if (ret == GST_FLOW_LOST_SYNC)
      goto done;
  }

  if (G_UNLIKELY (!stream->pes_buffer)) {
    /* set initial size of PES buffer */
    if (G_UNLIKELY (stream->pes_buffer_size == 0))
      stream->pes_buffer_size = FLUTS_MIN_PES_BUFFER_SIZE;

    stream->pes_buffer = gst_buffer_new_and_alloc (stream->pes_buffer_size);
    stream->pes_buffer_used = 0;
  }
  out_data = GST_BUFFER_DATA (stream->pes_buffer) + stream->pes_buffer_used;
#ifdef USE_LIBOIL
  oil_memcpy (out_data, in_data, in_size);
#else
  memcpy (out_data, in_data, in_size);
#endif
  stream->pes_buffer_used += in_size;
done:
  return ret;
}

static FORCE_INLINE GstFlowReturn
gst_fluts_demux_pes_buffer_flush (GstFluTSDemux * demux, gboolean discard)
{
  gint i;
  GstFlowReturn ret = GST_FLOW_OK;

  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];
    if (stream && stream->pad) {
      gst_fluts_stream_pes_buffer_flush (stream, discard);
      stream->pes_buffer_in_sync = FALSE;
    }
  }
  return ret;
}

static FORCE_INLINE GstFlowReturn
gst_fluts_demux_push_fragment (GstFluTSStream * stream,
    const guint8 * in_data, guint in_size)
{
  GstFlowReturn ret;
  GstBuffer *es_buf = gst_buffer_new_and_alloc (in_size);
#ifdef USE_LIBOIL
  oil_memcpy (GST_BUFFER_DATA (es_buf), in_data, in_size);
#else
  memcpy (GST_BUFFER_DATA (es_buf), in_data, in_size);
#endif
  ret = gst_pes_filter_push (&stream->filter, es_buf);

  /* If PES filter return ok then PES fragment buffering
   * can be enabled */
  if (ret == GST_FLOW_OK)
    stream->pes_buffer_in_sync = TRUE;
  else if (ret == GST_FLOW_LOST_SYNC)
    stream->pes_buffer_in_sync = FALSE;
  return ret;
}

static void
gst_fluts_demux_refresh_es_state (GstFluTSDemux * demux)
{
  gint i, j;
  GstFluTSStream *PMT_stream;
  GstFluTSStream *PCR_stream = NULL;
  gboolean found;

  PMT_stream = demux->streams[demux->current_PMT];
  if (PMT_stream == NULL) {
    GST_WARNING_OBJECT (demux, "Invalid PMT_stream");
    return;
  }

  PCR_stream = demux->streams[PMT_stream->PMT.PCR_PID];
  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];
    if (stream != NULL && stream->PID_type == PID_TYPE_ELEMENTARY) {
      found = FALSE;
      for (j = 0; j < PMT_stream->PMT.entries->len; j++) {
        GstFluTSPMTEntry *cur_entry =
            &g_array_index (PMT_stream->PMT.entries,
            GstFluTSPMTEntry, j);
        if (cur_entry->PID == stream->PID) {
          found = TRUE;
          break;
        }
      }

      if (!found) {
        /* Flush buffered PES data */
        gst_fluts_stream_pes_buffer_flush (stream, FALSE);
        gst_pes_filter_drain (&stream->filter);
        /* Resize the buffer to half if no overflow detected and
         * had been used less than half of it */
        if (stream->pes_buffer_overflow == FALSE
            && stream->pes_buffer_used < (stream->pes_buffer_size >> 1)) {
          stream->pes_buffer_size >>= 1;
          if (stream->pes_buffer_size < FLUTS_MIN_PES_BUFFER_SIZE)
            stream->pes_buffer_size = FLUTS_MIN_PES_BUFFER_SIZE;
          GST_DEBUG_OBJECT (demux, "PES buffer size reduced to %u bytes",
              stream->pes_buffer_size);
        }
        /* mark the stream not in sync to give a chance on PES filter to
         * detect lost sync */
        stream->pes_buffer_in_sync = FALSE;
        stream->pes_buffer_overflow = FALSE;
        stream->base_PCR = -1;
        stream->last_PCR = -1;
        stream->avg_PCR_difference = 0;
        stream->base_time = demux->pts_offset;
        stream->last_time = 0;
        stream->need_discont = TRUE;
        stream->PMT.version_number = -1;
        stream->PAT.version_number = -1;
        stream->PMT_pid = FLUTS_MAX_PID + 1;
        GST_DEBUG_OBJECT (demux, "stream with PID 0x%04x deactivated",
            stream->PID);
      }
      else if (stream->PMT_pid == FLUTS_MAX_PID + 1) {
        GST_DEBUG_OBJECT (demux, "stream with PID 0x%04x reactivated",
            stream->PID);
        stream->PMT_pid = PMT_stream->PID;
        stream->pes_buffer_in_sync = FALSE;
        stream->pes_buffer_overflow = FALSE;
        stream->base_PCR = -1;
        stream->last_PCR = -1;
        stream->avg_PCR_difference = 0;
        stream->base_time = demux->pts_offset;
        stream->last_time = 0;
        if (PCR_stream) {
          stream->base_time = PCR_stream->base_time;
        }
        stream->need_discont = TRUE;
      }
    }
  }
}

/*
 * transport_packet(){
 *   sync_byte                                                               8 bslbf == 0x47
 *   transport_error_indicator                                               1 bslbf
 *   payload_unit_start_indicator                                            1 bslbf
 *   transport _priority                                                     1 bslbf
 *   PID                                                                    13 uimsbf
 *   transport_scrambling_control                                            2 bslbf
 *   adaptation_field_control                                                2 bslbf
 *   continuity_counter                                                      4 uimsbf
 *   if(adaptation_field_control=='10' || adaptation_field_control=='11'){
 *     adaptation_field()
 *   }
 *   if(adaptation_field_control=='01' || adaptation_field_control=='11') {
 *     for (i=0;i<N;i++){
 *       data_byte                                                           8 bslbf
 *     }
 *   }
 * }
 */
static FORCE_INLINE GstFlowReturn
gst_fluts_demux_parse_stream (GstFluTSDemux * demux, GstFluTSStream * stream,
    const guint8 * in_data, guint in_size)
{
  GstFlowReturn ret;
  gboolean transport_error_indicator G_GNUC_UNUSED;
  gboolean payload_unit_start_indicator;
  gboolean transport_priority G_GNUC_UNUSED;
  guint16 PID;
  guint8 transport_scrambling_control G_GNUC_UNUSED;
  guint8 adaptation_field_control;
  guint8 continuity_counter;
  const guint8 *data = in_data;
  guint datalen = in_size;

  transport_error_indicator = (data[0] & 0x80) == 0x80;
  payload_unit_start_indicator = (data[0] & 0x40) == 0x40;
  transport_priority = (data[0] & 0x20) == 0x20;
  PID = stream->PID;
  transport_scrambling_control = (data[2] & 0xc0) >> 6;
  adaptation_field_control = (data[2] & 0x30) >> 4;
  continuity_counter = data[2] & 0x0f;

  data += 3;
  datalen -= 3;

  GST_LOG_OBJECT (demux, "afc 0x%x, pusi %d, PID 0x%04x datalen %u",
      adaptation_field_control, payload_unit_start_indicator, PID, datalen);

  ret = GST_FLOW_OK;

  /* packets with adaptation_field_control == 0 must be skipped */
  if (adaptation_field_control == 0)
    goto skip;

  /* parse adaption field if any */
  if (adaptation_field_control & 0x2) {
    guint consumed;

    if (!gst_fluts_demux_parse_adaptation_field (stream, data,
            datalen, &consumed))
      goto done;

    if (datalen <= consumed)
      goto too_small;

    data += consumed;
    datalen -= consumed;
    GST_LOG_OBJECT (demux, "consumed: %u datalen: %u", consumed, datalen);
  }

  /* If this packet has a payload, handle it */
  if (adaptation_field_control & 0x1) {
    GST_LOG_OBJECT (demux, "Packet payload %d bytes, PID 0x%04x", datalen, PID);

    /* For unknown streams, check if the PID is in the partial PIDs
     * list as an elementary stream and override the type if so
     */
    if (stream->PID_type == PID_TYPE_UNKNOWN) {
      if (fluts_is_elem_pid (demux, PID)) {
        GST_DEBUG_OBJECT (demux,
            "PID 0x%04x is an elementary stream in the PID list", PID);
        stream->PID_type = PID_TYPE_ELEMENTARY;
        stream->flags |= FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN;
        stream->base_time = demux->pts_offset;
        stream->last_time = 0;

        /* Clear any existing descriptor */
        if (stream->ES_info) {
          gst_mpeg_descriptor_free (stream->ES_info);
          stream->ES_info = NULL;
        }

        /* Initialise our PES filter */
        gst_pes_filter_init (&stream->filter, NULL, NULL);
        gst_pes_filter_set_callbacks (&stream->filter,
            (GstPESFilterData) gst_fluts_demux_data_cb,
            (GstPESFilterResync) gst_fluts_demux_resync_cb, stream);
      }
    }

    /* now parse based on the stream type */
    switch (stream->PID_type) {
      case PID_TYPE_PROGRAM_ASSOCIATION:
      case PID_TYPE_CONDITIONAL_ACCESS:
      case PID_TYPE_PROGRAM_MAP:
      case PID_TYPE_PRIVATE_SECTION:
      {
        GstBuffer *sec_buf;
        guint8 *section_data;
        guint16 section_length;
        guint8 pointer;

        /* do stuff with our section */
        if (payload_unit_start_indicator) {
          pointer = *data++;
          datalen -= 1;
          if (pointer >= datalen) {
            GST_DEBUG_OBJECT (demux, "pointer: 0x%02x too large", pointer);
            return GST_FLOW_OK;
          }
          data += pointer;
          datalen -= pointer;
        }

        /* FIXME: try to use data directly instead of creating a buffer and
           pushing in into adapter at section filter */
        sec_buf = gst_buffer_new_and_alloc (datalen);
#ifdef USE_LIBOIL
        oil_memcpy (GST_BUFFER_DATA (sec_buf), data, datalen);
#else
        memcpy (GST_BUFFER_DATA (sec_buf), data, datalen);
#endif
        if (gst_section_filter_push (&stream->section_filter,
                payload_unit_start_indicator, continuity_counter, sec_buf)) {
          GST_DEBUG_OBJECT (demux, "section finished");
          /* section ready */
          section_length = stream->section_filter.section_length;
          section_data =
              (guint8 *) gst_adapter_peek (stream->section_filter.adapter,
              section_length + 3);

          switch (stream->PID_type) {
            case PID_TYPE_PROGRAM_ASSOCIATION:
              gst_fluts_stream_parse_pat (stream, section_data,
                  section_length + 3);
              break;
            case PID_TYPE_CONDITIONAL_ACCESS:
              gst_fluts_stream_parse_cat (stream, section_data,
                  section_length + 3);
              break;
            case PID_TYPE_PROGRAM_MAP:
              gst_fluts_stream_parse_pmt (stream, section_data,
                  section_length + 3);
              break;
            case PID_TYPE_PRIVATE_SECTION:
              gst_fluts_stream_parse_private_section (stream, section_data,
                  section_length + 3);
              break;
          }

          gst_section_filter_clear (&stream->section_filter);

        } else {
          /* section still going, don't parse left */
          GST_DEBUG_OBJECT (demux, "section still going for PID 0x%04x", PID);
        }
        break;
      }
      case PID_TYPE_MPEG4_SECTION:
      {
        GstBuffer *section_buffer;
        guint16 section_length;
        guint8 pointer;

        /* do stuff with our section */
        if (payload_unit_start_indicator) {
          pointer = *data++;
          datalen -= 1;
          if (pointer >= datalen) {
            GST_DEBUG_OBJECT (demux, "pointer: 0x%02x too large", pointer);
            return GST_FLOW_OK;
          }
          data += pointer;
          datalen -= pointer;
        }

        section_buffer = gst_buffer_new_and_alloc (datalen);
        memcpy (GST_BUFFER_DATA (section_buffer), data, datalen);

        if (gst_section_filter_push (&stream->section_filter,
              payload_unit_start_indicator, continuity_counter,
              section_buffer)) {
          GstBuffer *msg_buffer;
          GST_DEBUG_OBJECT (demux,
              "MPEG4 section finished. Sending GstMessage.");
          /* section ready */
          section_length = stream->section_filter.section_length + 3;

          msg_buffer = gst_adapter_take_buffer (stream->section_filter.adapter,
              section_length);

          gst_element_post_message (GST_ELEMENT (demux),
              gst_message_new_element (GST_OBJECT (demux),
                  gst_structure_new ("mpeg4-section",
                      "content", GST_TYPE_BUFFER, msg_buffer,
                      "pid", G_TYPE_INT, PID,
                      NULL)));

          gst_buffer_unref (msg_buffer);

          gst_section_filter_clear (&stream->section_filter);

        } else {
          /* section still going, don't parse left */
          GST_DEBUG_OBJECT (demux, "MPEG4 section still going PID 0x%04x", PID);
        }
        break;
      }
      case PID_TYPE_NULL_PACKET:
        GST_DEBUG_OBJECT (demux,
            "skipping PID 0x%04x, type 0x%04x (NULL packet)", PID,
            stream->PID_type);
        break;
      case PID_TYPE_UNKNOWN:
        GST_DEBUG_OBJECT (demux, "skipping unknown PID 0x%04x, type 0x%04x",
            PID, stream->PID_type);
        break;
      case PID_TYPE_ELEMENTARY:
      {
        if (payload_unit_start_indicator) {
          GST_DEBUG_OBJECT (demux, "new PES start for PID 0x%04x, used %u"
              " bytes of %u bytes in the PES buffer",
              PID, stream->pes_buffer_used, stream->pes_buffer_size);
          /* Flush buffered PES data */
          gst_fluts_stream_pes_buffer_flush (stream, FALSE);
          gst_pes_filter_drain (&stream->filter);
          /* Also drain pending data in our CA stream */
          if (stream->CA_pid) {
            GstFluTSStream * ca_stream = NULL;

            ca_stream = gst_fluts_demux_get_stream_for_PID (demux, stream->CA_pid);

            if (ca_stream) {
              gst_fluts_stream_pes_buffer_flush (ca_stream, FALSE);
              gst_pes_filter_drain (&ca_stream->filter);
            }
          }
          /* Resize the buffer to half if no overflow detected and
           * had been used less than half of it */
          if (stream->pes_buffer_overflow == FALSE
              && stream->pes_buffer_used < (stream->pes_buffer_size >> 1)) {
            stream->pes_buffer_size >>= 1;
            if (stream->pes_buffer_size < FLUTS_MIN_PES_BUFFER_SIZE)
              stream->pes_buffer_size = FLUTS_MIN_PES_BUFFER_SIZE;
            GST_DEBUG_OBJECT (demux, "PES buffer size reduced to %u bytes",
                stream->pes_buffer_size);
          }
          /* mark the stream not in sync to give a chance on PES filter to
           * detect lost sync */
          stream->pes_buffer_in_sync = FALSE;
          stream->pes_buffer_overflow = FALSE;
        }
        GST_LOG_OBJECT (demux, "Elementary packet of size %u for PID 0x%04x",
            datalen, PID);

        if (datalen > 0) {
          /* Otherwise we buffer the PES fragment */
          ret = gst_fluts_stream_pes_buffer_push (demux, stream, data,
              datalen);
          /* If sync is lost here is due a pes_buffer_flush and we can try
           * to resync in the PES filter with the current fragment
           */
          if (ret == GST_FLOW_LOST_SYNC) {
            GST_LOG_OBJECT (demux, "resync, fragment pushed to PES filter");
            ret = gst_fluts_demux_push_fragment (stream, data, datalen);
          }
          break;
        }
        else {
          GST_WARNING_OBJECT (demux, "overflow of datalen: %u so skipping",
              datalen);
          return GST_FLOW_OK;
        }
      }
    }
  }

done:
  return ret;

skip:
  {
    GST_DEBUG_OBJECT (demux, "skipping, adaptation_field_control == 0");
    return GST_FLOW_OK;
  }
too_small:
  {
    GST_DEBUG_OBJECT (demux, "skipping, adaptation_field consumed all data");
    return GST_FLOW_OK;
  }
}

static FORCE_INLINE GstFlowReturn
gst_fluts_demux_parse_transport_packet (GstFluTSDemux * demux,
    const guint8 * data)
{
  GstFlowReturn ret = GST_FLOW_OK;
  guint16 PID;
  GstFluTSStream *stream;

  /* skip sync byte */
  data++;

  /* get PID */
  PID = ((data[0] & 0x1f) << 8) | data[1];

  /* Skip NULL packets */
  if (G_UNLIKELY (PID == 0x1fff))
    goto beach;

  /* get the stream. */
  stream = gst_fluts_demux_get_stream_for_PID (demux, PID);

  /* parse the stream */
  ret = gst_fluts_demux_parse_stream (demux, stream, data,
      FLUTS_NORMAL_TS_PACKETSIZE - 1);

  if (demux->pcr[1] != -1 && demux->bitrate == -1) {
    guint64 bitrate;
    GstClockTime tdelta = MPEGTIME_TO_GSTTIME (demux->pcr[1] - demux->pcr[0]);
    guint64 bytes = demux->packetsize * demux->num_packets;
    bitrate = gst_util_uint64_scale (GST_SECOND, bytes, tdelta);

    GST_DEBUG_OBJECT (demux, "pcr[0]:%" G_GUINT64_FORMAT
        " pcr[1]:%" G_GUINT64_FORMAT " time delta %" GST_TIME_FORMAT
        " bytes %" G_GUINT64_FORMAT " bitrate %" G_GUINT64_FORMAT " Bps",
        demux->pcr[0], demux->pcr[1], GST_TIME_ARGS (tdelta), bytes, bitrate);

    /* somehow... I doubt a bitrate below one packet per second is valid */
    if (bitrate > demux->packetsize - 1) {
      GstPad *peer;

      demux->bitrate = bitrate;
      GST_DEBUG_OBJECT (demux, "saved bitrate is %" G_GINT64_FORMAT
          " bytes per second", demux->bitrate);
      /* check that a duration can be queried */
      peer = gst_pad_get_peer (demux->sinkpad);
      if (peer) {
        GstQuery *query;
        gboolean res;
        gint64 duration = 0;

        /* Query peer for duration in bytes */
        query = gst_query_new_duration (GST_FORMAT_BYTES);
        res = gst_pad_query (peer, query);
        /* inform upstream that a duration has changed */
        if (res) {
          GstFormat format;

          /* Convert to time format */
          gst_query_parse_duration (query, &format, &duration);
              GST_DEBUG_OBJECT (demux, "query on peer pad reported bytes %"
                  G_GUINT64_FORMAT, duration);
          demux->cache_duration = BYTES_TO_GSTTIME (duration);
          GST_DEBUG_OBJECT (demux, "converted to time %" GST_TIME_FORMAT,
              GST_TIME_ARGS (demux->cache_duration));
          gst_element_post_message (GST_ELEMENT (demux), 
              gst_message_new_duration (GST_OBJECT (demux), GST_FORMAT_TIME,
                  demux->cache_duration));
        }
        gst_query_unref (query);
        gst_object_unref (peer);
      }
    } else {
      GST_WARNING_OBJECT (demux, "Couldn't compute valid bitrate, recomputing");
      demux->pcr[0] = demux->pcr[1] = -1;
      demux->num_packets = -1;
    }

    /* notify upstream that it can ask for seeking */
    gst_element_post_message (GST_ELEMENT (demux),
        gst_message_new_element (GST_OBJECT (demux),
            gst_structure_new ("seek", NULL)));
  }

beach:
  demux->num_packets++;
  return ret;

  /* ERRORS */
}

static gboolean
gst_fluts_demux_handle_seek_push (GstFluTSDemux * demux, GstEvent * event)
{
  gboolean res = FALSE;
  gdouble rate;
  GstFormat format;
  GstSeekFlags flags;
  GstSeekType start_type, stop_type;
  gint64 start, stop, bstart, bstop;
  GstEvent *bevent;

  gst_event_parse_seek (event, &rate, &format, &flags, &start_type, &start,
      &stop_type, &stop);

  GST_DEBUG_OBJECT (demux, "seek event, rate: %f start: %" GST_TIME_FORMAT
      " stop: %" GST_TIME_FORMAT, rate, GST_TIME_ARGS (start),
      GST_TIME_ARGS (stop));

  if (format == GST_FORMAT_BYTES) {
    GST_DEBUG_OBJECT (demux, "seek not supported on format %d", format);
    goto beach;
  }

  GST_DEBUG_OBJECT (demux, "seek - trying directly upstream first");

  /* first try original format seek */
  res = gst_pad_push_event (demux->sinkpad, gst_event_ref (event));
  if (res == TRUE)
    goto beach;
  GST_DEBUG_OBJECT (demux, "seek - no upstream");

  if (format != GST_FORMAT_TIME) {
    /* From here down, we only support time based seeks */
    GST_DEBUG_OBJECT (demux, "seek not supported on format %d", format);
    goto beach;
  }

  /* We need to convert to byte based seek and we need a scr_rate for that. */
  if (demux->bitrate == -1) {
    GST_DEBUG_OBJECT (demux, "seek not possible, no bitrate");
    goto beach;
  }

  GST_DEBUG_OBJECT (demux, "try with bitrate");

  bstart = GSTTIME_TO_BYTES (start);
  bstop = GSTTIME_TO_BYTES (stop);

  GST_DEBUG_OBJECT (demux, "in bytes bstart %" G_GINT64_FORMAT " bstop %"
      G_GINT64_FORMAT, bstart, bstop);
  bevent = gst_event_new_seek (rate, GST_FORMAT_BYTES, flags, start_type,
      bstart, stop_type, bstop);

  res = gst_pad_push_event (demux->sinkpad, bevent);

beach:
  gst_event_unref (event);
  return res;
}

static gboolean
gst_fluts_demux_src_event (GstPad * pad, GstEvent * event)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  gboolean res = FALSE;

  GST_DEBUG_OBJECT (demux, "got event %s",
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEEK:
      res = gst_fluts_demux_handle_seek_push (demux, event);
      break;
    default:
      res = gst_pad_push_event (demux->sinkpad, event);
      break;
  }

  gst_object_unref (demux);

  return res;
}

static void
gst_fluts_demux_flush (GstFluTSDemux * demux, gboolean discard)
{
  gint i;
  GstFluTSStream *PMT_stream;
  GstFluTSStream *PCR_stream;

  GST_DEBUG_OBJECT (demux, "flushing MPEG TS demuxer (discard %d)", discard);

  /* Start by flushing internal buffers */
  gst_fluts_demux_pes_buffer_flush (demux, discard);

  if (discard) {
    /* Clear adapter */
    gst_adapter_clear (demux->adapter);
  }

  /* Try resetting the last_PCR value as we will have a discont */
  if (demux->current_PMT == 0)
    goto beach;

  PMT_stream = demux->streams[demux->current_PMT];
  if (PMT_stream == NULL)
    goto beach;

  PCR_stream = demux->streams[PMT_stream->PMT.PCR_PID];
  if (PCR_stream == NULL)
    goto beach;

  PCR_stream->last_PCR = -1;

  /* Reset last time of all streams */
  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];

    if (stream) {
      /* Also reset the PES Filter state */
      gst_pes_filter_drain (&stream->filter);
      stream->last_time = 0;
      stream->need_discont = TRUE;
    }
  }

beach:
  return;
}

static gboolean
gst_fluts_demux_send_event (GstFluTSDemux * demux, GstEvent * event)
{
  gint i;
  gboolean have_stream = FALSE, res = TRUE;

  for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
    GstFluTSStream *stream = demux->streams[i];

    if (stream && stream->pad) {
      res &= gst_pad_push_event (stream->pad, gst_event_ref (event));
      have_stream = TRUE;
    }
  }
  gst_event_unref (event);

  return have_stream;
}

static gboolean
gst_fluts_demux_sink_event (GstPad * pad, GstEvent * event)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  gboolean res = FALSE;

  GST_DEBUG_OBJECT (demux, "got event %s",
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_FLUSH_START:
      res = gst_fluts_demux_send_event (demux, event);
      break;
    case GST_EVENT_FLUSH_STOP:
    {
      gint i;

      /* Start by flushing with a discard flag */
      gst_fluts_demux_flush (demux, TRUE);
      /* Now tell all streams they need a newsegment */
      for (i = 0; i < FLUTS_MAX_PID + 1; i++) {
        GstFluTSStream *stream = demux->streams[i];

        if (stream) {
          stream->need_segment = TRUE;
          stream->need_discont = TRUE;
          stream->base_PCR = -1;
          stream->base_time = demux->pts_offset;
        }
      }
      /* Reinitialize base_pts */
      demux->base_pts = GST_CLOCK_TIME_NONE;
      /* And finally kill any queued newsegment event */
      if (demux->new_seg_event) {
        gst_event_unref (demux->new_seg_event);
        demux->new_seg_event = NULL;
      }
      res = gst_fluts_demux_send_event (demux, event);

      demux->pushing = FALSE;
      break;
    }
    case GST_EVENT_EOS:
      gst_fluts_demux_flush (demux, FALSE);
      /* Send the EOS event on each stream */
      if (!(res = gst_fluts_demux_send_event (demux, event))) {
        /* we have no streams */
        GST_ELEMENT_ERROR (demux, STREAM, TYPE_NOT_FOUND,
            (NULL), ("No valid streams found at EOS"));
      }
      break;
    case GST_EVENT_NEWSEGMENT:
    {
      gboolean update;
      gdouble rate;
      GstFormat format;
      gint64 start, stop, time;

      gst_event_parse_new_segment (event, &update, &rate, &format,
          &start, &stop, &time);

      if (format == GST_FORMAT_TIME) {
        GST_INFO_OBJECT (demux, "received new segment: rate %g "
            "format %d, start: %" GST_TIME_FORMAT ", stop: %" GST_TIME_FORMAT
            ", time: %" GST_TIME_FORMAT, rate, format, GST_TIME_ARGS (start),
            GST_TIME_ARGS (stop), GST_TIME_ARGS (time));
      } else {
        GST_INFO_OBJECT (demux, "received new segment: rate %g "
            "format %d, start: %" G_GINT64_FORMAT ", stop: %" G_GINT64_FORMAT
            ", time: %" G_GINT64_FORMAT, rate, format, start, stop, time);
      }
      /* For BYTES newsegment we just store the estimated position in time
       * format. The demuxer will figure out the start and stop values from
       * the bitstream. We might want to store that requested position when
       * we see the seek event passing by for better accuracy. */
      if (format == GST_FORMAT_BYTES) {
        if (demux->bitrate != -1) {
          demux->requested_position = BYTES_TO_GSTTIME (time);
        }
        demux->requested_rate = rate;
      }
      /* For TIME newsegment we take that event as is and will relay it.
       * We assume that the upstream element that send that event knows
       * about the PCR/PTS values of the bistream at that position and that
       * our generated timestamps will match with the newsegment boundaries.
       * There's one exception to this case: When the newsegment looks like
       * a dummy time segment with start = 0 and stop = -1 we need to ignore
       * it and generate a proper newsegment as this one does not carry any
       * information. That's the case for udpsrc for example. */
      else if (format == GST_FORMAT_TIME && !(start == 0 && stop == -1)) {
        if (demux->new_seg_event) {
          gst_event_unref (demux->new_seg_event);
          demux->new_seg_event = NULL;
        }
        demux->new_seg_event = gst_event_ref (event);
      }
      res = TRUE;
      gst_event_unref (event);
      break;
    }
    default:
      res = gst_fluts_demux_send_event (demux, event);
      break;
  }
  gst_object_unref (demux);

  return res;
}

#ifdef HAVE_LATENCY
static gboolean
gst_fluts_demux_provides_clock (GstElement * element)
{
  GstFluTSDemux *demux;
  GstQuery *query;
  gboolean is_live = FALSE;
  GstPad *peer;

  demux = GST_FLUTS_DEMUX (element);
  query = gst_query_new_latency ();
  peer = gst_pad_get_peer (demux->sinkpad);

  if (peer) {
    if (gst_pad_query (peer, query))
      gst_query_parse_latency (query, &is_live, NULL, NULL);
    gst_object_unref (peer);
  }
  gst_query_unref (query);
  
  demux->is_live = is_live;

  return is_live;
}

static GstClock *
gst_fluts_demux_provide_clock (GstElement * element)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (element);

  if (gst_fluts_demux_provides_clock (element)) {
    if (demux->clock == NULL) {
      demux->clock = g_object_new (GST_TYPE_SYSTEM_CLOCK, "name",
          "FluTSClock", NULL);
      demux->clock_base = GST_CLOCK_TIME_NONE;
    }
    return gst_object_ref (demux->clock);
  }

  return NULL;
}
#endif

static const GstQueryType *
gst_fluts_demux_src_pad_query_type (GstPad * pad)
{
  static const GstQueryType types[] = {
    GST_QUERY_LATENCY,
    GST_QUERY_DURATION,
    GST_QUERY_SEEKING,
    0
  };

  return types;
}

static gboolean
gst_fluts_demux_src_pad_query (GstPad * pad, GstQuery * query)
{
  GstFluTSDemux * demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  gboolean res = FALSE;
  GstPad * peer;

  peer = NULL;

  switch (GST_QUERY_TYPE (query)) {
#ifdef HAVE_LATENCY
    case GST_QUERY_LATENCY:
    {
      peer = gst_pad_get_peer (demux->sinkpad);
      if (peer) {
        res = gst_pad_query (peer, query);
        if (res) {
          gboolean is_live;
          GstClockTime min_latency, max_latency;

          gst_query_parse_latency (query, &is_live, &min_latency, &max_latency);
          if (is_live) {
            min_latency += TS_LATENCY * GST_MSECOND;
            if (max_latency != GST_CLOCK_TIME_NONE)
              max_latency += TS_LATENCY * GST_MSECOND;
          }

          gst_query_set_latency (query, is_live, min_latency, max_latency);
        }
        gst_object_unref (peer);
      }
      break;
    }
#endif
    case GST_QUERY_DURATION:
    {
      GstFormat format;
      GstPad *peer;

      gst_query_parse_duration (query, &format, NULL);

      /* Try query upstream first */
      peer = gst_pad_get_peer (demux->sinkpad);
      if (peer) {
        res = gst_pad_query (peer, query);
        /* Try doing something with that query if it failed */
        if (!res && format == GST_FORMAT_TIME && demux->bitrate != -1) {
          /* Try using cache first */
          if (GST_CLOCK_TIME_IS_VALID (demux->cache_duration)) {
            GST_LOG_OBJECT (demux, "replying duration query from cache %"
                GST_TIME_FORMAT, GST_TIME_ARGS (demux->cache_duration));
            gst_query_set_duration (query, GST_FORMAT_TIME,
                demux->cache_duration);
            res = TRUE;
          } else {              /* Query upstream and approximate */
            GstQuery *bquery = gst_query_new_duration (GST_FORMAT_BYTES);
            gint64 duration = 0;

            /* Query peer for duration in bytes */
            res = gst_pad_query (peer, bquery);
            if (res) {
              /* Convert to time format */
              gst_query_parse_duration (bquery, &format, &duration);
              GST_DEBUG_OBJECT (demux, "query on peer pad reported bytes %"
                  G_GUINT64_FORMAT, duration);
              demux->cache_duration = BYTES_TO_GSTTIME (duration);
              GST_DEBUG_OBJECT (demux, "converted to time %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (demux->cache_duration));
              gst_query_set_duration (query, GST_FORMAT_TIME,
                  demux->cache_duration);
            }
            gst_query_unref (bquery);
          }
        }
        gst_object_unref (peer);
      }
      break;
    }
    case GST_QUERY_SEEKING:{
      GstFormat fmt;

      gst_query_parse_seeking (query, &fmt, NULL, NULL, NULL);
      if (fmt == GST_FORMAT_BYTES) {
        /* Seeking in BYTES format not supported at all */
        gst_query_set_seeking (query, fmt, FALSE, -1, -1);
      } else {
        GstQuery *peerquery;
        GstPad * peer;
        gboolean seekable;

        /* Then ask upstream */
        if ((peer = gst_pad_get_peer (demux->sinkpad)) == NULL) {
          GST_DEBUG_OBJECT (demux, "seeking not possible, no peer");
          goto beach;
        }
        res = gst_pad_query (peer, query);
        if (res) {
          /* If upstream can handle seeks we're done, if it
           * can't we still have our TIME->BYTES conversion seek
           */
          gst_query_parse_seeking (query, NULL, &seekable, NULL, NULL);
          if (seekable || fmt != GST_FORMAT_TIME) {
            gst_object_unref (peer);
            goto beach;
          }
        }

        /* We can't say anything about seekability if we didn't
         * have a second PCR yet because the bitrate is calculated
         * from this
         */
        if (demux->bitrate == -1 && demux->pcr[1] == -1) {
          gst_object_unref (peer);
          goto beach;
        }

        /* We can seek if upstream supports BYTES seeks and we
         * have a bitrate
         */
        peerquery = gst_query_new_seeking (GST_FORMAT_BYTES);
        res = gst_pad_query (peer, peerquery);
        if (!res || demux->bitrate == -1) {
          gst_query_set_seeking (query, fmt, FALSE, -1, -1);
        } else {
          gst_query_parse_seeking (peerquery, NULL, &seekable, NULL, NULL);
          if (seekable)
            gst_query_set_seeking (query, GST_FORMAT_TIME, TRUE, 0, -1);
          else
            gst_query_set_seeking (query, fmt, FALSE, -1, -1);
        }
        gst_object_unref (peer);
        gst_query_unref (peerquery);
        res = TRUE;
      }
      break;
    }
    default:
      res = gst_pad_query_default (pad, query);
      break;
  }

beach:
  gst_object_unref (demux);

  return res;
}

static FORCE_INLINE gint
is_mpegts_sync (const guint8 * in_data, const guint8 * end_data,
    guint packetsize)
{
  guint ret = 0;
  if (G_LIKELY (IS_MPEGTS_SYNC (in_data)))
    return 100;

  if (in_data + packetsize < end_data - 5) {
    if (G_LIKELY (IS_MPEGTS_SYNC (in_data + packetsize)))
      ret += 50;
  }

  if (in_data[0] == 0x47) {
    ret += 25;

    if ((in_data[1] & 0x80) == 0x00)
      ret += 10;

    if ((in_data[3] & 0x10) == 0x10)
      ret += 5;
  }
  return ret;
}

static inline void
gst_fluts_demux_detect_packet_size (GstFluTSDemux * demux, guint len)
{
  guint i, packetsize;
  guint max_freq_cnt, max_freq_bin;

  guint freq_dist[FLUTS_NUM_PACKETSIZES];
  memset(freq_dist, 0, sizeof(freq_dist));

  for (i = 1; i < len; i++) {
    packetsize = demux->sync_lut[i] - demux->sync_lut[i - 1];
    if (packetsize == FLUTS_NORMAL_TS_PACKETSIZE)
      freq_dist[0]++;
    else if (packetsize == FLUTS_M2TS_TS_PACKETSIZE)
      freq_dist[1]++;
    else if (packetsize == FLUTS_DVB_ASI_TS_PACKETSIZE)
      freq_dist[2]++;
    else if (packetsize == FLUTS_ATSC_TS_PACKETSIZE)
      freq_dist[3]++;
  }

  /* Search the most frequent size */
  max_freq_cnt = 0;
  max_freq_bin = -1;
  for (i = 0; i < FLUTS_NUM_PACKETSIZES; i++) {
    if (freq_dist[i] > max_freq_cnt) {
      max_freq_cnt = freq_dist[i];
      max_freq_bin = i;
    }
  }

  packetsize = 0;
  switch (max_freq_bin) {
  case 0:
    packetsize = FLUTS_NORMAL_TS_PACKETSIZE;
    break;
  case 1:
    packetsize = FLUTS_M2TS_TS_PACKETSIZE;
    break;
  case 2:
    packetsize = FLUTS_DVB_ASI_TS_PACKETSIZE;
    break;
  case 3:
    packetsize = FLUTS_ATSC_TS_PACKETSIZE;
    break;
  }

  demux->packetsize = (packetsize ? packetsize : FLUTS_NORMAL_TS_PACKETSIZE);
  GST_DEBUG_OBJECT (demux, "packet_size set to %d bytes", demux->packetsize);
}

static guint
gst_fluts_demux_sync_scan (GstFluTSDemux * demux, const guint8 * in_data,
    guint size, guint * flush)
{
  guint sync_count = 0;
  guint8 *ptr_data = (guint8 *) in_data;
  guint packetsize =
      (demux->packetsize ? demux->packetsize : FLUTS_NORMAL_TS_PACKETSIZE);
  const guint8 *end_scan = in_data + size - packetsize;

  /* Check if the LUT table is big enough */
  if (G_UNLIKELY (demux->sync_lut_len < (size / packetsize))) {
    demux->sync_lut_len = size / packetsize;
    if (demux->sync_lut)
      g_free (demux->sync_lut);
    demux->sync_lut = g_new0 (guint8 *, demux->sync_lut_len);
    GST_DEBUG_OBJECT (demux, "created sync LUT table with %u entries",
        demux->sync_lut_len);
  }

  while (ptr_data < end_scan && sync_count < demux->sync_lut_len) {
    /* if sync code is found try to store it in the LUT */
    guint chance = is_mpegts_sync (ptr_data, end_scan, packetsize);
    if (demux->packetsize) {
      if (G_LIKELY (chance > 50)) {
        /* skip paketsize bytes and try find next */
        guint8 *next_sync = ptr_data + packetsize;
        if (next_sync < end_scan) {
          demux->sync_lut[sync_count] = ptr_data;
          sync_count++;
          ptr_data += packetsize;
        } else
          goto done;
      } else {
        ptr_data++;
      }
    }
    else {
      /* At first scan the real packet size might be 0 then we
       * want to discover it and the heuristic approach cannot be used
       */
      if (G_LIKELY (chance > 0)) {
        demux->sync_lut[sync_count] = ptr_data;
        sync_count++;
        ptr_data += packetsize;
      } else {
        ptr_data++;
      }
    }
  }
done:

  if (G_UNLIKELY (!demux->packetsize))
    gst_fluts_demux_detect_packet_size (demux, sync_count);

  *flush = MIN (size, ptr_data - in_data);

  /* Now that we discovered a packetsize redo the the sync LUT to
   * avoid having invalid entries */
  if (demux->first_sync_scan && demux->packetsize)
  {
      demux->first_sync_scan = FALSE;
      sync_count = gst_fluts_demux_sync_scan (demux, in_data, size, flush);
  }

  return sync_count;
}

static GstFlowReturn
gst_fluts_demux_chain (GstPad * pad, GstBuffer * buffer)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (gst_pad_get_parent (pad));
  GstFlowReturn ret = GST_FLOW_OK;
  const guint8 *data;
  guint avail;
  guint flush = 0;
  gint i;
  guint sync_count;

  if (GST_BUFFER_IS_DISCONT (buffer)) {
    gst_fluts_demux_flush (demux, FALSE);
  }
  /* first push the new buffer into the adapter */
  gst_adapter_push (demux->adapter, buffer);

  /* check if there's enough data to parse a packets we want a min of 3
   * to be able autodetect the packetsize */
  avail = gst_adapter_available (demux->adapter);
  if (G_UNLIKELY (avail < 3 * FLUTS_NORMAL_TS_PACKETSIZE))
    goto done;

  /* recover all data from adapter */
  data = gst_adapter_peek (demux->adapter, avail);

  /* scan for sync codes */
  sync_count = gst_fluts_demux_sync_scan (demux, data, avail, &flush);

  /* process all packets */
  for (i = 0; i < sync_count; i++) {
    ret = gst_fluts_demux_parse_transport_packet (demux, demux->sync_lut[i]);
    if (G_UNLIKELY (ret == GST_FLOW_LOST_SYNC
            || ret == GST_FLOW_NEED_MORE_DATA)) {
      ret = GST_FLOW_OK;
      continue;
    }
    if (G_UNLIKELY (ret != GST_FLOW_OK)) {
      flush = demux->sync_lut[i] - data + demux->packetsize;
      flush = MIN (avail, flush);
      goto done;
    }
  }

done:
  /* flush processed data */
  if (flush) {
    GST_DEBUG_OBJECT (demux, "flushing %d/%d", flush, avail);
    gst_adapter_flush (demux->adapter, flush);
  }

  gst_object_unref (demux);

  return ret;
}

static GstStateChangeReturn
gst_fluts_demux_change_state (GstElement * element, GstStateChange transition)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (element);
  GstStateChangeReturn result;


  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      demux->adapter = gst_adapter_new ();
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      break;
    default:
      break;
  }

  result = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      if (demux->adapter) {
        gst_adapter_clear (demux->adapter);
      }
      gst_fluts_demux_reset (demux);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      if (demux->adapter) {
        g_object_unref (demux->adapter);
        demux->adapter = NULL;
      }
      if (demux->sync_lut)
        g_free (demux->sync_lut);
      demux->sync_lut = NULL;
      demux->sync_lut_len = 0;
      break;
    default:
      break;
  }

  return result;
}

static GValueArray *
fluts_demux_build_pat_info (GstFluTSDemux * demux)
{
  GValueArray *vals = NULL;
  GstFluTSPAT *PAT;
  gint i;

  g_return_val_if_fail (demux->streams[0] != NULL, NULL);
  g_return_val_if_fail (demux->streams[0]->PID_type ==
      PID_TYPE_PROGRAM_ASSOCIATION, NULL);

  PAT = &(demux->streams[0]->PAT);
  vals = g_value_array_new (PAT->entries->len);

  for (i = 0; i < PAT->entries->len; i++) {
    GstFluTSPATEntry *cur_entry =
        &g_array_index (PAT->entries, GstFluTSPATEntry, i);
    GValue v = { 0, };
    FluTsPatInfo *info_obj;

    info_obj = fluts_pat_info_new (cur_entry->program_number, cur_entry->PID);

    g_value_init (&v, G_TYPE_OBJECT);
    g_value_take_object (&v, info_obj);
    g_value_array_append (vals, &v);
  }
  return vals;
}

static FluTsPmtInfo *
fluts_demux_build_pmt_info (GstFluTSDemux * demux, guint16 pmt_pid)
{
  FluTsPmtInfo *info_obj;
  GstFluTSPMT *PMT;
  gint i;

  g_return_val_if_fail (demux->streams[pmt_pid] != NULL, NULL);
  g_return_val_if_fail (demux->streams[pmt_pid]->PID_type ==
      PID_TYPE_PROGRAM_MAP, NULL);

  PMT = &(demux->streams[pmt_pid]->PMT);

  info_obj = fluts_pmt_info_new (PMT->program_number, PMT->PCR_PID,
      PMT->version_number);

  if (PMT->program_info) {
    for (i = 0; i < gst_mpeg_descriptor_n_desc (PMT->program_info); ++i) {
      guint8 *desc = gst_mpeg_descriptor_nth (PMT->program_info, i);

      /* add the whole descriptor, tag + length + DESC_LENGTH bytes */
      fluts_pmt_info_add_descriptor (info_obj,
          (gchar *) desc, 2 + DESC_LENGTH (desc));
    }
  }

  for (i = 0; i < PMT->entries->len; i++) {
    GstFluTSStream *stream;
    FluTsPmtStreamInfo *stream_info;
    GstFluTSPMTEntry *cur_entry =
        &g_array_index (PMT->entries, GstFluTSPMTEntry, i);

    stream = demux->streams[cur_entry->PID];
    stream_info =
        fluts_pmt_stream_info_new (cur_entry->PID, stream->stream_type);

    if (stream->ES_info) {
      int i;

      /* add languages */
      guint8 *iso639_languages =
          gst_mpeg_descriptor_find (stream->ES_info, DESC_ISO_639_LANGUAGE);
      if (iso639_languages) {
        for (i = 0; i < DESC_ISO_639_LANGUAGE_codes_n (iso639_languages); i++) {
          gchar *language_n = (gchar *)
              DESC_ISO_639_LANGUAGE_language_code_nth (iso639_languages, i);
          fluts_pmt_stream_info_add_language (stream_info,
              g_strndup (language_n, 3));
        }
      }

      for (i = 0; i < gst_mpeg_descriptor_n_desc (stream->ES_info); ++i) {
        guint8 *desc = gst_mpeg_descriptor_nth (stream->ES_info, i);

        /* add the whole descriptor, tag + length + DESC_LENGTH bytes */
        fluts_pmt_stream_info_add_descriptor (stream_info,
            (gchar *) desc, 2 + DESC_LENGTH (desc));
      }
    }
    fluts_pmt_info_add_stream (info_obj, stream_info);
  }
  return info_obj;
}

static void
gst_fluts_demux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (object);
  gchar **pids;
  guint num_pids;
  int i;

  switch (prop_id) {
    case PROP_ES_PIDS:
      pids = g_strsplit (g_value_get_string (value), ":", -1);
      num_pids = g_strv_length (pids);
      if (num_pids > 0) {
        demux->elementary_pids = g_new0 (guint16, num_pids);
        demux->nb_elementary_pids = num_pids;
        for (i = 0; i < num_pids; i++) {
          demux->elementary_pids[i] = strtol (pids[i], NULL, 0);
          GST_INFO ("partial TS ES pid %d", demux->elementary_pids[i]);
        }
      }
      g_strfreev (pids);
      break;
    case PROP_CHECK_CRC:
      demux->check_crc = g_value_get_boolean (value);
      break;
    case PROP_PROGRAM_NUMBER:
      demux->program_number = g_value_get_int (value);
      break;
    case PROP_MAX_PES_BUFFER_SIZE:
      demux->max_pes_buffer_size = g_value_get_int (value);
      break;
    case PROP_FORCE_PCR_SEGMENT:
      demux->force_pcr_segment = g_value_get_boolean (value);
      break;
    case PROP_PCR_DIFFERENCE:
      demux->pcr_difference = g_value_get_int (value);
      break;
    case PROP_PTS_OFFSET:
      demux->pts_offset = g_value_get_uint64 (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_fluts_demux_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFluTSDemux *demux = GST_FLUTS_DEMUX (object);
  int i;

  switch (prop_id) {
    case PROP_ES_PIDS:
      if (demux->nb_elementary_pids == 0) {
        g_value_set_static_string (value, "");
      } else {
        gchar **ts_pids;

        ts_pids = g_new0 (gchar *, demux->nb_elementary_pids + 1);
        for (i = 0; i < demux->nb_elementary_pids; i++) {
          ts_pids[i] = g_strdup_printf ("%d", demux->elementary_pids[i]);
        }

        g_value_set_string (value, g_strjoinv (":", ts_pids));
        g_strfreev (ts_pids);
      }
      break;
    case PROP_CHECK_CRC:
      g_value_set_boolean (value, demux->check_crc);
      break;
    case PROP_PROGRAM_NUMBER:
      g_value_set_int (value, demux->program_number);
      break;
    case PROP_PAT_INFO:
    {
      if (demux->streams[0] != NULL) {
        g_value_take_boxed (value, fluts_demux_build_pat_info (demux));
      }
      break;
    }
    case PROP_PMT_INFO:
    {
      if (demux->current_PMT != 0 && demux->streams[demux->current_PMT] != NULL) {
        g_value_take_object (value, fluts_demux_build_pmt_info (demux,
                demux->current_PMT));
      }
      break;
    }
    case PROP_MAX_PES_BUFFER_SIZE:
      g_value_set_int (value, demux->max_pes_buffer_size);
      break;
    case PROP_FORCE_PCR_SEGMENT:
      g_value_set_boolean (value, demux->force_pcr_segment);
      break;
    case PROP_PCR_DIFFERENCE:
      g_value_set_int (value, demux->pcr_difference);
      break;
    case PROP_PTS_OFFSET:
      g_value_set_uint64 (value, demux->pts_offset);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

gboolean
gst_fluts_demux_plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "flutsdemux",
          GST_RANK_PRIMARY+1, GST_TYPE_FLUTS_DEMUX))
    return FALSE;

  return TRUE;
}
