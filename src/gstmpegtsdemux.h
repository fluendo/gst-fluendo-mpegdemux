/* 
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is Fluendo MPEG Demuxer plugin.
 *
 * The Initial Developer of the Original Code is Fluendo, S.A.
 * Portions created by Fluendo, S.A. are Copyright (C) 2005, 2006, 2007, 2008, 2009
 * Fluendo, S.A. All Rights Reserved.
 *
 */

#ifndef __GST_FLUTS_DEMUX_H__
#define __GST_FLUTS_DEMUX_H__

#include "gst-compat.h"

#include "gstmpegdesc.h"
#include "gstpesfilter.h"
#include "gstsectionfilter.h"

G_BEGIN_DECLS

#if GST_CHECK_VERSION (0,10,12)
#define HAVE_LATENCY
#endif

#define FLUTS_MIN_PES_BUFFER_SIZE     4 * 1024
#define FLUTS_MAX_PES_BUFFER_SIZE   256 * 1024
#define FLUTS_MAX_PID 0x1fff
#define FLUTS_NORMAL_TS_PACKETSIZE  188
#define FLUTS_M2TS_TS_PACKETSIZE    192
#define FLUTS_DVB_ASI_TS_PACKETSIZE 204
#define FLUTS_ATSC_TS_PACKETSIZE    208
#define FLUTS_NUM_PACKETSIZES       4

#define IS_MPEGTS_SYNC(data) (((data)[0] == 0x47) && \
                                    (((data)[1] & 0x80) == 0x00) && \
                                    (((data)[3] & 0x10) == 0x10))
#define GST_TYPE_FLUTS_DEMUX              (gst_fluts_demux_get_type())
#define GST_FLUTS_DEMUX(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj),\
                                            GST_TYPE_FLUTS_DEMUX,GstFluTSDemux))
#define GST_FLUTS_DEMUX_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass),\
                                            GST_TYPE_FLUTS_DEMUX,GstFluTSDemuxClass))
#define GST_FLUTS_DEMUX_GET_CLASS(klass)  (G_TYPE_INSTANCE_GET_CLASS((klass),\
                                            GST_TYPE_FLUTS_DEMUX,GstFluTSDemuxClass))
#define GST_IS_FLUTS_DEMUX(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj),\
                                            GST_TYPE_FLUTS_DEMUX))
#define GST_IS_FLUTS_DEMUX_CLASS(obj)     (G_TYPE_CHECK_CLASS_TYPE((klass),\
                                            GST_TYPE_FLUTS_DEMUX))
typedef struct _GstFluTSStream GstFluTSStream;
typedef struct _GstFluTSPMTEntry GstFluTSPMTEntry;
typedef struct _GstFluTSPMT GstFluTSPMT;
typedef struct _GstFluTSPATEntry GstFluTSPATEntry;
typedef struct _GstFluTSPAT GstFluTSPAT;
typedef struct _GstFluTSCAT GstFluTSCAT;
typedef struct _GstFluTSDemux GstFluTSDemux;
typedef struct _GstFluTSDemuxClass GstFluTSDemuxClass;

struct _GstFluTSPMTEntry
{
  guint16 PID;
};

struct _GstFluTSPMT
{
  guint16 program_number;
  guint8 version_number;
  gboolean current_next_indicator;
  guint8 section_number;
  guint8 last_section_number;
  guint16 PCR_PID;
  guint16 program_info_length;
  GstMPEGDescriptor *program_info;

  GArray *entries;
};

struct _GstFluTSPATEntry
{
  guint16 program_number;
  guint16 PID;
};

struct _GstFluTSPAT
{
  guint16 transport_stream_id;
  guint8 version_number;
  gboolean current_next_indicator;
  guint8 section_number;
  guint8 last_section_number;

  GArray *entries;
};

struct _GstFluTSCAT
{
  guint16 transport_stream_id;
  guint8 version_number;
  gboolean current_next_indicator;
  guint8 section_number;
  guint8 last_section_number;
};

typedef enum _FluTsStreamFlags
{
  FLUTS_STREAM_FLAG_STREAM_TYPE_UNKNOWN = 0x01,
  FLUTS_STREAM_FLAG_PMT_VALID = 0x02,
  FLUTS_STREAM_FLAG_IS_VIDEO = 0x04,
  FLUTS_STREAM_FLAG_IS_ECM = 0x08,
  FLUTS_STREAM_FLAG_IS_EMM = 0x10
} FluTsStreamFlags;

typedef enum {
  FLUTS_STREAM_ACTIVATED, /* This stream has been activated already */
  FLUTS_STREAM_ACTIVATE_ON_DATA, /* Will be marked for activation when data arrives */
  FLUTS_STREAM_NOT_ACTIVATED /* Stream disabled */
} FluTsStreamActivation;

/* Information associated to a single MPEG stream. */
struct _GstFluTSStream
{
  GstFluTSDemux *demux;

  FluTsStreamFlags flags;
  FluTsStreamActivation activation;

  /* PID and type */
  guint16 PID;
  guint8 PID_type;

  /* adaptation_field data */
  guint64 last_PCR;
  guint64 base_PCR;
  guint64 last_OPCR;
  guint64 avg_PCR_difference;
  gboolean discont_PCR;
  GstClockTimeDiff discont_difference;

  /* for PAT streams */
  GstFluTSPAT PAT;

  /* for PMT streams */
  GstFluTSPMT PMT;

  /* for CA streams */
  GstFluTSCAT CAT;

  /* for PAT, PMT, CA and private streams */
  GstSectionFilter section_filter;

  /* for PES streams */
  guint8 id;
  guint8 stream_type;
  GstBuffer *pes_buffer;
  guint32 pes_buffer_size;
  guint32 pes_buffer_used;
  gboolean pes_buffer_overflow;
  gboolean pes_buffer_in_sync;
  GstPESFilter filter;

  gchar *name;
  GstPadTemplate *template;
  GstPad *pad;
  GstCaps *caps;

  GstFlowReturn last_ret;
  GstMPEGDescriptor *ES_info;

  
  /* for tags */
  const gchar *codec;
  guint component_tag;
  gboolean is_encrypted;
  gboolean for_hearing_impaired;

  /* needed because 33bit mpeg timestamps wrap around every (approx) 26.5 hrs */
  GstClockTimeDiff base_time;
  GstClockTime last_time;

  /* pid of PMT that this stream belongs to */
  guint16 PMT_pid;
  guint16 CA_pid;

  gboolean need_discont; /* reflects that DISCONT flag have to be set */
  gboolean need_segment;
  gboolean is_valid_for_segment; /* PTS data is usable for segment definition */
};

struct _GstFluTSDemux
{
  GstElement parent;

  /* properties */
  gboolean check_crc;
  gboolean force_pcr_segment;

  /* sink pad and adapter */
  GstPad *sinkpad;
  GstAdapter *adapter;
  guint8 **sync_lut;
  guint sync_lut_len;
  gboolean first_sync_scan;

  /* current PMT PID */
  guint16 current_PMT;

  /* Array of FLUTS_MAX_PID + 1 stream entries */
  GstFluTSStream **streams;
  /* Array to perform pmts checks at gst_fluts_demux_parse_adaptation_field */
  gboolean pmts_checked[FLUTS_MAX_PID + 1];

  /* Array of Elementary Stream pids for ts with PMT */
  guint16 *elementary_pids;
  guint nb_elementary_pids;

  /* Program number to use */
  gint program_number;

  /* Indicates we are being used by playbin2 */
  gboolean playbin2;
  
  /* indicates that we need to close our pad group, because we've added
   * at least one pad */
  gboolean need_no_more_pads;
  /* Indicate that we've started pushing data */
  gboolean pushing;
  gint packetsize;
  gint max_pes_buffer_size;

#ifdef HAVE_LATENCY
  /* clocking */
  GstClock *clock;
  GstClockTime clock_base;
  gboolean is_live;
#endif

  /* Additional information required for seeking */
  guint64 num_packets;
  gint64 bitrate;

  /* Two PCRs observations to calculate bitrate */
  guint64 pcr[2];
  guint pcr_difference;

  /* Cached duration estimation */
  GstClockTime cache_duration;

  /* Cached base_PCR in GStreamer time. */
  GstClockTime base_pts;

  /* This is used to define an initial offset for PTS which can be useful
   * to handle reverse playback with rollovers */
  GstClockTime pts_offset;

  /* Requested position/rate for seeks */
  GstClockTime requested_position;
  gdouble requested_rate;

  /* New segment event for downstream pads */
  GstEvent *new_seg_event;
};

struct _GstFluTSDemuxClass
{
  GstElementClass parent_class;

  GstPadTemplate *sink_template;
  GstPadTemplate *video_template;
  GstPadTemplate *audio_template;
  GstPadTemplate *subpicture_template;
  GstPadTemplate *private_template;
  GstPadTemplate *mpeg4sl_template;
};

GType gst_fluts_demux_get_type (void);

gboolean gst_fluts_demux_plugin_init (GstPlugin * plugin);

#define AVCOMPONENT_TAG_COMPONENT_TAG "av-component-tag"
#define AVCOMPONENT_TAG_PID "av-pid"
#define AVCOMPONENT_TAG_IS_ENCRYPTED "av-is-encrypted"
#define AVCOMPONENT_TAG_AUDIO_DESCRIPTION "av-audio-description"
#define AVCOMPONENT_TAG_HEARING_IMPAIRED "av-hearing-impaired"
#define AVCOMPONENT_TAG_ASPECT_RATIO "av-aspect-ratio"

G_END_DECLS
#endif /* __GST_FLUTS_DEMUX_H__ */
