/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Defined if gcov is enabled to force a rebuild due to config.h changing */
/* #undef GST_GCOV_ENABLED */

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define if valgrind should be used */
/* #undef HAVE_VALGRIND */

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "gst-fluendo-mpegdemux"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "GStreamer Fluendo MPEG Demuxer"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "GStreamer Fluendo MPEG Demuxer 0.10.85"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "gst-fluendo-mpegdemux"

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.10.85"

/* directory where plugins are located */
#define PLUGINDIR "/usr/local/lib/gstreamer-0.10"

/* GStreamer version is >= 0.10.10 */
#define POST_10_10 1

/* GStreamer version is >= 0.10.11 */
#define POST_10_11 1

/* GStreamer version is >= 0.10.12 */
#define POST_10_12 1

/* GStreamer version is >= 0.10.13 */
#define POST_10_13 1

/* GStreamer version is >= 0.10.14 */
#define POST_10_14 1

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Build liboil code paths */
/* #undef USE_LIBOIL */

/* Version number of package */
#define VERSION "0.10.85"
